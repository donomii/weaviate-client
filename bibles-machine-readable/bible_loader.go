package main

// Download data from: https://www.biblesupersearch.com/wp-content/uploads/2022/01/all_bibles_json_5.0.zip

import (
        "context"
        "encoding/json"
        "flag"
        "fmt"
        "io/ioutil"
        "log"
        "os"
        "path/filepath"

        "github.com/weaviate/weaviate-go-client/v4/weaviate"
        "github.com/weaviate/weaviate/entities/models"
)

type Metadata struct {
        Name             string `json:"name"`
        Shortname        string `json:"shortname"`
        Module           string `json:"module"`
        Year             string `json:"year"`
        Publisher        string `json:"publisher"`
        Owner            string `json:"owner"`
        Description      string `json:"description"`
        Lang             string `json:"lang"`
        LangShort        string `json:"lang_short"`
        Copyright        int    `json:"copyright"`
        CopyrightStatement string `json:"copyright_statement"`
        URL              string `json:"url"`
        CitationLimit    int    `json:"citation_limit"`
        Restrict         int    `json:"restrict"`
        Italics          int    `json:"italics"`
        Strongs          int    `json:"strongs"`
        RedLetter        int    `json:"red_letter"`
        Paragraph        int    `json:"paragraph"`
        Official         int    `json:"official"`
        Research         int    `json:"research"`
        ModuleVersion    string `json:"module_version"`
}

type Verse struct {
        BookName string `json:"book_name"`
        Book     int    `json:"book"`
        Chapter  int    `json:"chapter"`
        Verse    int    `json:"verse"`
        Text     string `json:"text"`
}

type BibleData struct {
        Metadata Metadata `json:"metadata"`
        Verses   []Verse  `json:"verses"`
}


func boolRef(b bool) *bool {
        return &b
}

func verseSchema(tokenizer string) *models.Class {
        return &models.Class{
                Class: "Verse",
                Properties: []*models.Property{
                        {
                                IndexFilterable: boolRef(true),
                                IndexSearchable: boolRef(true),
                                Name:            "bookName",
                                DataType:        []string{"text"},
                                Tokenization:    tokenizer,
                        },
                        {
                                IndexFilterable: boolRef(true),
                                IndexSearchable: boolRef(false),
                                Name:            "book",
                                DataType:        []string{"int"},
                        },
                        {
                                IndexFilterable: boolRef(true),
                                IndexSearchable: boolRef(false),
                                Name:            "chapter",
                                DataType:        []string{"int"},
                        },
                        {
                                IndexFilterable: boolRef(true),
                                IndexSearchable: boolRef(false),
                                Name:            "verse",
                                DataType:        []string{"int"},
                        },
                        {
                                IndexFilterable: boolRef(true),
                                IndexSearchable: boolRef(true),
                                Name:            "text",
                                DataType:        []string{"text"},
                                Tokenization:    tokenizer,
                        },
                        {
                                IndexFilterable: boolRef(true),
                                IndexSearchable: boolRef(true),
                                Name:            "bibleName",
                                DataType:        []string{"text"},
                                Tokenization: tokenizer,
                        },
                },
        }
}

func createSchema(client *weaviate.Client, tokenizer string) error {
        ctx := context.Background()

        // Delete existing class if it exists
        err := client.Schema().ClassDeleter().WithClassName("Verse").Do(ctx)
        if err != nil {
                log.Printf("Error deleting existing class (if it exists): %v", err)
        }

        verseClass := verseSchema(tokenizer)

        err = client.Schema().ClassCreator().WithClass(verseClass).Do(ctx)
        if err != nil {
                return err
        }
        return nil
}

func main() {
        // Command line flags
        host := flag.String("host", "localhost:8080", "Weaviate host address")
        scheme := flag.String("scheme", "http", "Weaviate scheme (http or https)")
        directory := flag.String("directory", "data", "Directory containing JSON files to process")

        tokenizer := flag.String("tokenizer", "word", "Tokenizer to use (word, gse, kagome_jp)")
        flag.Parse()

        // Weaviate client configuration
        cfg := weaviate.Config{
                Host:   *host,
                Scheme: *scheme,
        }
        client, err := weaviate.NewClient(cfg)
        if err != nil {
                log.Fatalf("Failed to connect to Weaviate: %v", err)
        }

        err = createSchema(client, *tokenizer)
        if err != nil {
                log.Fatalf("Failed to create schema: %v", err)
        }

        log.Println("Schema created successfully.")

        // Process json files and load data
        err = filepath.Walk(*directory, func(path string, info os.FileInfo, err error) error {
                if err != nil {
                        return err
                }
                if !info.IsDir() && filepath.Ext(path) == ".json" {
                        err := processFile(client, path)
                        if err != nil {
                                return err
                        }
                }
                return nil
        })

        if err != nil {
                log.Fatalf("Error processing files: %v", err)
        }

        fmt.Println("Processing completed.")
}

func processFile(client *weaviate.Client, filePath string) error {
        data, err := ioutil.ReadFile(filePath)
        if err != nil {
                return fmt.Errorf("failed to read file: %v", err)
        }

        var bibleData BibleData
        err = json.Unmarshal(data, &bibleData)
        if err != nil {
                return fmt.Errorf("failed to parse JSON: %v", err)
        }

        objects := make([]*models.Object, len(bibleData.Verses))
        for i, verse := range bibleData.Verses {
                objects[i] = &models.Object{
                        Class: "Verse",
                        Properties: map[string]interface{}{
                                "bookName":  verse.BookName,
                                "book":      verse.Book,
                                "chapter":   verse.Chapter,
                                "verse":     verse.Verse,
                                "text":      verse.Text,
                                "bibleName": bibleData.Metadata.Name,
                        },
                }
        }

        batcher := client.Batch().ObjectsBatcher()
        for _, obj := range objects {
                batcher = batcher.WithObject(obj)
        }

        ctx := context.Background()
        if _, err := batcher.Do(ctx); err != nil {
                return fmt.Errorf("failed to import objects into Weaviate: %v", err)
        }

        fmt.Printf("Processed file: %s\n", filePath)
        return nil
}
