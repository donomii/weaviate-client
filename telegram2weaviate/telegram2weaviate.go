package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"time"

	"gitlab.com/donomii/weaviate-client"
)

type TextEntity struct {
	Type string `json:"type"`
	Text string `json:"text"`
}

type Message struct {
	ID           int64        `json:"id"`
	Type         string       `json:"type"`
	Date         string       `json:"date"`
	DateUnixTime string       `json:"date_unixtime"`
	Actor        string       `json:"actor,omitempty"`
	ActorID      string       `json:"actor_id,omitempty"`
	Action       string       `json:"action,omitempty"`
	From         string       `json:"from,omitempty"`
	FromID       string       `json:"from_id,omitempty"`
	Text         interface{}  `json:"text,omitempty"` // can be string or []TextEntity
}

type Chat struct {
	Name     string    `json:"name"`
	Type     string    `json:"type"`
	ID       int64     `json:"id"`
	Messages []Message `json:"messages"`
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage: telegram2weaviate <chat.json>")
		os.Exit(1)
	}

	file := os.Args[1]

	// Read and parse JSON file
	data, err := ioutil.ReadFile(file)
	if err != nil {
		fmt.Printf("Failed to read file: %v\n", err)
		return
	}

	var chat Chat
	err = json.Unmarshal(data, &chat)
	if err != nil {
		fmt.Printf("Failed to parse JSON: %v\n", err)
		return
	}

	const pageSize = 30
	const overlapSize = 15

	// Process messages in pages with overlap
	for start := 0; start < len(chat.Messages); start += pageSize - overlapSize {
		end := start + pageSize
		if end > len(chat.Messages) {
			end = len(chat.Messages)
		}
		page := chat.Messages[start:end]

		// Create a single text string with formatted messages
		messagesText := formatMessages(page)

		// Use details from the first message for Weaviate properties
		firstMessage := page[0]
		properties := map[string]interface{}{
			"chatName":    chat.Name,
			"chatType":    chat.Type,
			"chatID":      chat.ID,
			"firstMessageID": firstMessage.ID,
			"firstMessageDate": firstMessage.Date,
			"messages":    messagesText,
		}

		object := weaviate.ObjectInsert{
			Class:      "Messages",
			Properties: properties,
		}

		insertObj := weaviate.ObjectsInsert{
			Objects: []weaviate.ObjectInsert{object},
		}

		_, err := weaviate.InsertObjects(insertObj)
		if err != nil {
			fmt.Printf("Error submitting to Weaviate: %s\n", err)
		}
	}
}

// formatMessages concatenates messages into a single formatted string
func formatMessages(messages []Message) string {
	var result string
	for _, msg := range messages {
		timestamp := formatTimestamp(msg.Date)
		username := msg.From
		if username == "" {
			username = msg.Actor
		}
		messageText := extractText(msg.Text)
		result += fmt.Sprintf("%s %s: %s\n", timestamp, username, messageText)
	}
	return result
}

// extractText handles different formats of the text field
func extractText(text interface{}) string {
	switch v := text.(type) {
	case string:
		return v
	case []interface{}:
		// Concatenate each text entity if it's a list
		var textParts string
		for _, entity := range v {
			if textEntity, ok := entity.(map[string]interface{}); ok {
				textParts += textEntity["text"].(string) + " "
			}
		}
		return textParts
	default:
		return ""
	}
}

// formatTimestamp formats the date from ISO format to a simple timestamp
func formatTimestamp(isoDate string) string {
	t, err := time.Parse(time.RFC3339, isoDate)
	if err != nil {
		return isoDate // fallback to the original if parsing fails
	}
	return t.Format("2006-01-02 15:04:05")
}