mkdir nfcorpus
cd nfcorpus
aria2c https://huggingface.co/datasets/BeIR/nfcorpus/resolve/main/corpus.jsonl.gz -o corpus.jsonl.gz
gunzip corpus.jsonl.gz &
aria2c https://huggingface.co/datasets/BeIR/nfcorpus/resolve/main/queries.jsonl.gz -o queries.jsonl.gz
gunzip queries.jsonl.gz &
cd ..

mkdir msmarco
cd msmarco
aria2c https://msmarco.z22.web.core.windows.net/msmarcoranking/msmarco-doctrain-queries.tsv.gz -o msmarco-doctrain-queries.tsv.gz &

aria2c https://msmarco.z22.web.core.windows.net/msmarcoranking/msmarco-doctrain-qrels.tsv.gz -o msmarco-doctrain-qrels.tsv.gz &

aria2c https://msmarco.z22.web.core.windows.net/msmarcoranking/msmarco-docs.tsv.gz -o msmarco-docs.tsv.gz &

wait

gunzip msmarco-docs.tsv.gz &
gunzip msmarco-doctrain-queries.tsv.gz &
gunzip msmarco-doctrain-qrels.tsv.gz &

wait