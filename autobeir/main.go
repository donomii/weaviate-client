package main

import (
	"context"
	_ "embed"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"time"

	"log"
	"strings"

	"github.com/weaviate/weaviate-go-client/v4/weaviate"
	"github.com/weaviate/weaviate-go-client/v4/weaviate/graphql"
	"github.com/weaviate/weaviate/entities/models"
	"net/url"
)

var BatchSize = 1000
var maxInserts = 1000000
var completedInserts = 0
var queriedLines = 0
var noMarco = false
var noQuery = false
var Vectorizer = "text2vec-bigram"
var Tokenization = "word"
var Replicas = 4

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	log.Println("Start")

	var serverURL string
	var noLoad bool
	flag.StringVar(&serverURL, "server-url", "http://localhost:8080", "URL of the server to connect to")
	flag.BoolVar(&noLoad, "no-load", false, "Don't load the data, just run the queries")
	flag.IntVar(&BatchSize, "batch-size", BatchSize, "Batch size for loading data")
	flag.IntVar(&maxInserts, "max-inserts", maxInserts, "Maximum number of inserts")
	flag.BoolVar(&noMarco, "no-marco", false, "Don't run the marco test")
	flag.BoolVar(&noQuery, "no-query", false, "Don't run the query test")
	flag.StringVar(&Vectorizer, "vectorizer", Vectorizer, "Vectorizer to use")
	flag.StringVar(&Tokenization, "tokenization", Tokenization, "Tokenization to use")
	flag.IntVar(&Replicas, "replicas", Replicas, "Number of replicas to use")

	flag.Parse()

	parsed, err := url.Parse(serverURL)
	panicIfErr(err)


	log.Printf("Started\n")
	config := weaviate.Config{
		Scheme: parsed.Scheme,
		Host:   parsed.Host,
	}

	fmt.Printf("Batch size %v, maximum documents %v\n", BatchSize, maxInserts)

	client := weaviate.New(config)

	_, err = client.Misc().LiveChecker().Do(context.Background())
	panicIfErr(err)



	log.Printf("Connected to %s\n", serverURL)

	if !noLoad {
		err := client.Schema().AllDeleter().Do(context.Background())
		panicIfErr(err)
		log.Printf("Deleted all data\n")
	}
	//nfcorpus(serverURL, noLoad)
	if !noMarco {
		if err := client.Schema().ClassCreator().
			WithClass(msmarcoSchema()).
			Do(context.Background()); err != nil {
			fmt.Printf("create schema for msmarco: %v", err)
		}
		time.Sleep(3)  //>.>
		log.Printf("Created schema for msmarco")
		msmarco(serverURL, noLoad)
	}

	log.Println("Finished")
}

// nfcorpus line: {"_id": "MED-10", "title": "Statin Use and Breast Cancer Survival: A Nationwide Cohort Study from Finland", "text": "Recent studies have suggested that statins, an established drug group in the prevention of cardiovascular mortality, could delay or prevent breast cancer recurrence but the effect on disease-specific mortality remains unclear. We evaluated risk of breast cancer death among statin users in a population-based cohort of breast cancer patients. The study cohort included all newly diagnosed breast cancer patients in Finland during 1995\u20132003 (31,236 cases), identified from the Finnish Cancer Registry. Information on statin use before and after the diagnosis was obtained from a national prescription database. We used the Cox proportional hazards regression method to estimate mortality among statin users with statin use as time-dependent variable. A total of 4,151 participants had used statins. During the median follow-up of 3.25 years after the diagnosis (range 0.08\u20139.0 years) 6,011 participants died, of which 3,619 (60.2%) was due to breast cancer. After adjustment for age, tumor characteristics, and treatment selection, both post-diagnostic and pre-diagnostic statin use were associated with lowered risk of breast cancer death (HR 0.46, 95% CI 0.38\u20130.55 and HR 0.54, 95% CI 0.44\u20130.67, respectively). The risk decrease by post-diagnostic statin use was likely affected by healthy adherer bias; that is, the greater likelihood of dying cancer patients to discontinue statin use as the association was not clearly dose-dependent and observed already at low-dose/short-term use. The dose- and time-dependence of the survival benefit among pre-diagnostic statin users suggests a possible causal effect that should be evaluated further in a clinical trial testing statins\u2019 effect on survival in breast cancer patients.", "metadata": {"url": "http://www.ncbi.nlm.nih.gov/pubmed/25329299"}}

type nfcorpusLine struct {
	ID       string `json:"_id"`
	Title    string `json:"title"`
	Text     string `json:"text"`
	Metadata struct {
		URL string `json:"url"`
	} `json:"metadata"`
}

/* nfcorpus query doc: [
    {
        "queryID": "PLAIN-3",
        "query": "Breast Cancer Cells Feed on Cholesterol",
        "matchingDocIDs": [
            1379,
            1380,
            1381,
            1382,
            1383,
            1371,
            1372,
            1373,
            1374,
            1375,
            1376,
            1377,
            1378
			],
			"matchingDoc_text": [
				"The content of low density lipoprotein (LDL) receptors in tissue from primary breast cancers was determined and its prognostic information compared with that of variables of established prognostic importance. Frozen tumour specimens were selected, and tissue from 72 patients (32 of whom had died) were studied. The LDL receptor content showed an inverse correlation with the survival time. Analysis by a multivariate statistical method showed that the presence of axillary metastasis, content of receptors for oestrogen and LDL, diameter of the tumour, and DNA pattern were all of prognostic value with regard to patient survival. Improved methods of predicting survival time in patients with breast cancer may be of value in the choice of treatment for individual patients.",
				],
				"original_matchingDocIDs": [
					"MED-2436",
					"MED-2437",
					"MED-2438"
					]
				},
			}

*/

type nfcorpusQueryElem struct {
	QueryID                string   `json:"queryID"`
	Query                  string   `json:"query"`
	MatchingDocIDs         []int    `json:"matchingDocIDs"`
	MatchingDocText        []string `json:"matchingDoc_text"`
	OriginalMatchingDocIDs []string `json:"original_matchingDocIDs"`
}

type nfcorpusQueryDoc []nfcorpusQueryElem

// Embed nfcorpus
var nfcorpusBlob []byte

// Embed nfcorpus queries
var nfcorpusQueriesBlob []byte

func loadNfCorpus(path string) []nfcorpusLine {
	var out []nfcorpusLine
	var data []byte
	var err error
	if path == "" {
		data = nfcorpusBlob
	} else {
		data, err = ioutil.ReadFile(path)
		panicIfErr(err)
	}
	//Split the file into lines
	lines := strings.Split(string(data), "\n")
	//Iterate over the lines
	for _, line := range lines {
		//Parse the line into a struct
		var parsed nfcorpusLine
		err := json.Unmarshal([]byte(line), &parsed)
		if err != nil {
			if fmt.Sprintf("%v", err) != "unexpected end of JSON input" {
				fmt.Printf("Error parsing line: %v\n", err)
				fmt.Printf("Line: %v\n", line)
			}
		}
		//Append the parsed line to the output
		out = append(out, parsed)
	}
	return out
}

func loadNfCorpusQueries(path string) nfcorpusQueryDoc {
	var out nfcorpusQueryDoc
	var data []byte
	var err error
	if path == "" {
		data = nfcorpusQueriesBlob
	} else {
		data, err = ioutil.ReadFile(path)
		panicIfErr(err)
	}
	err = json.Unmarshal([]byte(data), &out)
	if err != nil {
		fmt.Printf("Error parsing nfqueries: %v\n", err)

	}
	return out
}

func nfcorpusSchema() *models.Class {
	return &models.Class{
		Class: "nfcorpus",
		Properties: []*models.Property{
			&models.Property{
				Name:         "title",
				DataType:     []string{"text"},
				Tokenization: "word",
			},
			&models.Property{
				Name:         "text",
				DataType:     []string{"text"},
				Tokenization: "word",
			},
			&models.Property{
				Name:         "corpusid",
				DataType:     []string{"text"},
				Tokenization: "field",
			},
		},
		Vectorizer: "text2vec-bigram",
	}
}

func nfcorpus(serverURL string, noLoad bool) {

	parsed, err := url.Parse(serverURL)
	panicIfErr(err)


	config := weaviate.Config{
		Scheme: parsed.Scheme,
		Host:   parsed.Host,
	}

	client := weaviate.New(config)

	_, err = client.Misc().LiveChecker().Do(context.Background())
	panicIfErr(err)

	log.Printf("Connected to %s\n", serverURL)

	if !noLoad {

		nfcorpuslines := loadNfCorpus("nfcorpus/corpus.jsonl")

		log.Printf("Loaded %d data items from nfcorpus", len(nfcorpuslines))

		if err := client.Schema().ClassCreator().
			WithClass(nfcorpusSchema()).
			Do(context.Background()); err != nil {
			fmt.Printf("create schema for nfcorpus: %v", err)
		}
		log.Printf("Created schema for nfcorpus")

		batch := client.Batch().ObjectsBatcher()
		for i, line := range nfcorpuslines {
			completedInserts = completedInserts + 1
			props := map[string]interface{}{
				"title":    line.Title,
				"text":     line.Text,
				"corpusid": fmt.Sprintf("%d", i),
			}
			batch.WithObjects(&models.Object{
				Class:      "nfcorpus",
				Properties: props,
			})

			if i != 0 && i%BatchSize == 0 {
				br, err := batch.Do(context.Background())
				if err != nil {
					log.Printf("Error detected in batch: %v\n", err)
				}

				for i, r := range br {
					if r.Result.Errors != nil {
						for _, err := range r.Result.Errors.Error {
							fmt.Printf("Error in item %d: %v\n", i, err.Message)
						}
					}
				}



			}
			if completedInserts > maxInserts {
				break
			}
		}

		log.Printf("Imported %d data items from nfcorpus", len(nfcorpuslines))
	}

	if noQuery {
		return
	}

	nfcorpusquerylines := loadNfCorpusQueries("nfcorpus/queries.jsonl")

	log.Printf("Loaded %d queries from nfcorpus", len(nfcorpusquerylines))
	var count1 int
	var count5 int
	var count10 int
	for _, line := range nfcorpusquerylines {
		queryBuilder := client.GraphQL().Get().WithClassName("Nfcorpus").WithLimit(100).WithFields(graphql.Field{Name: "_additional { id }"}, graphql.Field{Name: "text"}, graphql.Field{Name: "corpusid"})
		/*
			bm25 := &graphql.BM25ArgumentBuilder{}
			bm25.WithQuery(line.Query)
			queryBuilder.WithBM25(bm25)
		*/
		hybrid := client.GraphQL().HybridArgumentBuilder()
		hybrid.WithQuery(line.Query)
		hybrid.WithAlpha(1.0)
		queryBuilder.WithHybrid(hybrid)

		//fmt.Printf("query: %v", line.Query)
		result, err := queryBuilder.Do(context.Background())
		panicIfErr(err)

		for _, err := range result.Errors {
			fmt.Printf("error: %v\n", err.Message)
		}

		//compare results to expected results in line
		resultIds := result.Data["Get"].(map[string]interface{})["Nfcorpus"].([]interface{})
		for i, id := range resultIds {

			mid := id.(map[string]interface{})
			//fmt.Printf("expected: %v\n", line.MatchingDocIDs)
			//fmt.Printf("id %v, answer: %v\n", mid["corpusid"], mid["text"])

			for _, expected_id := range line.MatchingDocIDs {
				text_id := fmt.Sprintf("%v", mid["corpusid"])
				text_expected := fmt.Sprintf("%d", expected_id)

				if text_id == text_expected {
					//fmt.Println("Matched: ", text_id, text_expected)
					if i < 1 {
						count1++
					}
					if i < 5 {
						count5++
					}
					if i < 10 {
						count10++
					}
				}
			}
		}
		queriedLines += 1
		if queriedLines > maxInserts {
			break
		}
	}
	log.Printf("Ran %d queries from nfcorpus", len(nfcorpusquerylines))
	fmt.Printf("hits: @1: %v, @5: %v, @10: %v\n", count1, count5, count10)

}
