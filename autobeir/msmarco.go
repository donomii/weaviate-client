package main

import (
	"context"
	"encoding/gob"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/weaviate/weaviate-go-client/v4/weaviate"
	"github.com/weaviate/weaviate-go-client/v4/weaviate/graphql"
	"github.com/weaviate/weaviate/entities/models"
)

// ---------------- Main MSMARCO Function ----------------

func msmarco(serverURL string, noLoad bool) {
	parsed, err := url.Parse(serverURL)
	panicIfErr(err)

	log.Printf("Started\n")
	config := weaviate.Config{
		Scheme: parsed.Scheme,
		Host:   parsed.Host,
	}

	client := weaviate.New(config)

	_, err = client.Misc().LiveChecker().Do(context.Background())
	panicIfErr(err)

	log.Printf("Connected to %s\n", serverURL)

	// Use our streaming cache loaders:
	msmarcoRelationLines := loadMsMarcoRelations("msmarco/msmarco-doctrain-qrels.tsv")
	log.Printf("Loaded %d relations from msmarco", len(msmarcoRelationLines))

	msmarcoQueries := loadMsMarcoQueries("msmarco/msmarco-doctrain-queries.tsv")
	log.Printf("Loaded %d queries from msmarco", len(msmarcoQueries))

	var msmarcoCorpusEntries map[string]msmarcoCorpusLine
	var startTime time.Time

	if !noLoad {
		msmarcoCorpusEntries = loadMsMarcoCorpus("msmarco/msmarco-docs.tsv")
		log.Printf("Loaded %d corpus entries from msmarco data file", len(msmarcoCorpusEntries))

		log.Printf("Starting to load documents into weaviate now...")
		startTime = time.Now()

		batch := client.Batch().ObjectsBatcher()
		i := 0
		completedInserts := 0

		// Only load corpus entries that are referenced in the relation lines.
		for _, rel := range msmarcoRelationLines {
			for _, r := range rel {
				completedInserts++
				line := msmarcoCorpusEntries[r]

				props := map[string]interface{}{
					"url":      line.Url,
					"title":    line.Title,
					"text":     line.Text,
					"corpusid": line.ID,
				}
				batch.WithObjects(&models.Object{
					Class:      "Msmarco",
					Properties: props,
				})

				if i != 0 && i%BatchSize == 0 {
					br, err := batch.Do(context.Background())
					if err != nil {
						log.Printf("Error detected in batch: %v\n", err)
					}
					for j, r := range br {
						if r.Result.Errors != nil {
							for _, err := range r.Result.Errors.Error {
								log.Printf("Error in corpus item %d: %v\n", j, err.Message)
							}
						}
					}
				}
				i++
			}
			if completedInserts > maxInserts {
				break
			}
		}
		elapsed := time.Since(startTime)
		log.Printf("Loaded %d documents from msmarco in %v seconds", completedInserts, elapsed.Seconds())
	} else {
		// If not loading documents, we still need corpus entries for later querying.
		msmarcoCorpusEntries = loadMsMarcoCorpus("msmarco/msmarco-docs.tsv")
	}

	if noQuery {
		return
	}

	// Dump some data to make sure it is in sync
	j := 0
	for topicId, docIDs := range msmarcoRelationLines {
		j++
		if j > 10 {
			break
		}
		query := msmarcoQueries[topicId]
		fmt.Printf("Query %v: %v\n", topicId, query.Text)
		for _, q := range docIDs {
			answer := msmarcoCorpusEntries[q]
			fmt.Printf("Expected answer: %v\n", answer.Text)
		}
		fmt.Println()
	}

	// Query the data and calculate nDCG.
	var ndcg5, ndcg10, queriedLines int
	for topicId, relation := range msmarcoRelationLines {
		expectedAnswersIDs := relation
		queryStruct := msmarcoQueries[topicId]
		fmt.Printf("%v: query: %v\n", topicId, queryStruct.Text)

		var expectedAnswerStructs []msmarcoCorpusLine
		for _, expectedID := range expectedAnswersIDs {
			answer := msmarcoCorpusEntries[expectedID]
			fmt.Printf("Expected answer id %v\n", expectedID)
			expectedAnswerStructs = append(expectedAnswerStructs, answer)
		}

		queryBuilder := client.GraphQL().Get().
			WithClassName("Msmarco").
			WithLimit(10).
			WithFields(
				graphql.Field{Name: "_additional { id }"},
				graphql.Field{Name: "text"},
				graphql.Field{Name: "corpusid"},
			)
		bm25 := &graphql.BM25ArgumentBuilder{}
		bm25.WithQuery(queryStruct.Text)
		queryBuilder.WithBM25(bm25)

		result, err := queryBuilder.Do(context.Background())
		panicIfErr(err)

		for _, err := range result.Errors {
			fmt.Printf("error: %v\n", err.Message)
		}
		// Compare results to expected results.
		resultIdsI := result.Data["Get"].(map[string]interface{})["Msmarco"]
		if resultIdsI == nil {
			fmt.Printf("no results found\n")
			continue
		}
		resultIds := resultIdsI.([]interface{})

		for resultIndex, res := range resultIds {
			answerIDi := res.(map[string]interface{})["corpusid"]
			answerID := answerIDi.(string)
			answerStruct := msmarcoCorpusEntries[answerID]
			fmt.Printf("answer id: %v\n", answerID)
			fmt.Printf("answer text: %v\n", answerStruct.Text)
			for _, expectedAnswer := range expectedAnswerStructs {
				fmt.Printf("Expected answer id: %v, answer id %v\n", expectedAnswer.ID, answerID)
				if expectedAnswer.ID == answerID {
					if resultIndex < 5 {
						ndcg5++
						fmt.Printf("top5 ")
					}
					if resultIndex < 10 {
						ndcg10++
						fmt.Printf("top10 ")
					}
				}
			}
		}
		fmt.Println()
		fmt.Printf("nDCG@5: %v, nDCG@10: %v\n", ndcg5, ndcg10)
		queriedLines++
		if queriedLines > maxInserts {
			break
		}
	}
	fmt.Printf("nDCG@5: %v, nDCG@10: %v", ndcg5, ndcg10)
}

// ---------------- Data Structures ----------------

type msmarcoCorpusLine struct {
	ID    string `json:"id"`
	Url   string `json:"url"`
	Title string `json:"title"`
	Text  string `json:"text"`
}

type msmarcoQueryLine struct {
	ID   string `json:"id"`
	Text string `json:"text"`
}

// ---------------- Cache-Enabled Load Functions with Streaming ----------------

// fileExists returns true if the given filename exists.
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// ====== Corpus Cache Helpers ======

func saveCorpusCache(cachePath string, data map[string]msmarcoCorpusLine) error {
	f, err := os.Create(cachePath)
	if err != nil {
		return err
	}
	defer f.Close()
	enc := gob.NewEncoder(f)

	// Write the count first.
	if err := enc.Encode(len(data)); err != nil {
		return err
	}
	// Stream each key–value pair.
	for key, value := range data {
		if err := enc.Encode(key); err != nil {
			return err
		}
		if err := enc.Encode(value); err != nil {
			return err
		}
	}
	return nil
}

func loadCorpusCache(cachePath string) (map[string]msmarcoCorpusLine, error) {
	f, err := os.Open(cachePath)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	dec := gob.NewDecoder(f)

	var count int
	if err := dec.Decode(&count); err != nil {
		return nil, err
	}
	out := make(map[string]msmarcoCorpusLine, count)
	for i := 0; i < count; i++ {
		var key string
		var value msmarcoCorpusLine
		if err := dec.Decode(&key); err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		if err := dec.Decode(&value); err != nil {
			return nil, err
		}
		out[key] = value
	}
	return out, nil
}

// loadMsMarcoCorpus first checks for a cached file using a streaming encoder/decoder.
func loadMsMarcoCorpus(path string) map[string]msmarcoCorpusLine {
	cachePath := path + ".cache"
	if fileExists(cachePath) {
		if cached, err := loadCorpusCache(cachePath); err == nil {
			log.Printf("Loaded corpus from cache: %s", cachePath)
			return cached
		} else {
			log.Printf("Error decoding corpus cache: %v", err)
		}
	}

	// Parse the TSV file if no valid cache exists.
	out := make(map[string]msmarcoCorpusLine)
	data, err := ioutil.ReadFile(path)
	panicIfErr(err)
	lines := strings.Split(string(data), "\n")
	for i, line := range lines {
		if len(strings.TrimSpace(line)) == 0 {
			continue
		}
		fields := strings.Split(line, "\t")
		if len(fields) < 4 {
			fmt.Printf("Error in corpus line %d: %v\n", i, line)
			continue
		}
		parsed := msmarcoCorpusLine{
			ID:    fields[0],
			Url:   fields[1],
			Title: fields[2],
			Text:  fields[3],
		}
		out[parsed.ID] = parsed
	}

	if err := saveCorpusCache(cachePath, out); err != nil {
		log.Printf("Error saving corpus cache: %v", err)
	} else {
		log.Printf("Saved corpus to cache: %s", cachePath)
	}

	return out
}

// ====== Queries Cache Helpers ======

func saveQueriesCache(cachePath string, data map[string]msmarcoQueryLine) error {
	f, err := os.Create(cachePath)
	if err != nil {
		return err
	}
	defer f.Close()
	enc := gob.NewEncoder(f)

	if err := enc.Encode(len(data)); err != nil {
		return err
	}
	for key, value := range data {
		if err := enc.Encode(key); err != nil {
			return err
		}
		if err := enc.Encode(value); err != nil {
			return err
		}
	}
	return nil
}

func loadQueriesCache(cachePath string) (map[string]msmarcoQueryLine, error) {
	f, err := os.Open(cachePath)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	dec := gob.NewDecoder(f)

	var count int
	if err := dec.Decode(&count); err != nil {
		return nil, err
	}
	out := make(map[string]msmarcoQueryLine, count)
	for i := 0; i < count; i++ {
		var key string
		var value msmarcoQueryLine
		if err := dec.Decode(&key); err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		if err := dec.Decode(&value); err != nil {
			return nil, err
		}
		out[key] = value
	}
	return out, nil
}

// loadMsMarcoQueries first checks for a cached file using streaming.
func loadMsMarcoQueries(path string) map[string]msmarcoQueryLine {
	cachePath := path + ".cache"
	if fileExists(cachePath) {
		if cached, err := loadQueriesCache(cachePath); err == nil {
			log.Printf("Loaded queries from cache: %s", cachePath)
			return cached
		} else {
			log.Printf("Error decoding queries cache: %v", err)
		}
	}

	out := make(map[string]msmarcoQueryLine)
	data, err := ioutil.ReadFile(path)
	panicIfErr(err)
	lines := strings.Split(string(data), "\n")
	for _, line := range lines {
		if len(strings.TrimSpace(line)) == 0 {
			continue
		}
		fields := strings.Split(line, "\t")
		if len(fields) < 2 {
			fmt.Printf("Error in query line: %v\n", line)
			continue
		}
		parsed := msmarcoQueryLine{
			ID:   fields[0],
			Text: fields[1],
		}
		out[parsed.ID] = parsed
	}

	if err := saveQueriesCache(cachePath, out); err != nil {
		log.Printf("Error saving queries cache: %v", err)
	} else {
		log.Printf("Saved queries to cache: %s", cachePath)
	}
	return out
}

// ====== Relations Cache Helpers ======

func saveRelationsCache(cachePath string, data map[string][]string) error {
	f, err := os.Create(cachePath)
	if err != nil {
		return err
	}
	defer f.Close()
	enc := gob.NewEncoder(f)

	if err := enc.Encode(len(data)); err != nil {
		return err
	}
	for key, value := range data {
		if err := enc.Encode(key); err != nil {
			return err
		}
		if err := enc.Encode(value); err != nil {
			return err
		}
	}
	return nil
}

func loadRelationsCache(cachePath string) (map[string][]string, error) {
	f, err := os.Open(cachePath)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	dec := gob.NewDecoder(f)

	var count int
	if err := dec.Decode(&count); err != nil {
		return nil, err
	}
	out := make(map[string][]string, count)
	for i := 0; i < count; i++ {
		var key string
		var value []string
		if err := dec.Decode(&key); err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		if err := dec.Decode(&value); err != nil {
			return nil, err
		}
		out[key] = value
	}
	return out, nil
}

// loadMsMarcoRelations first checks for a cached file using streaming.
func loadMsMarcoRelations(path string) map[string][]string {
	cachePath := path + ".cache"
	if fileExists(cachePath) {
		if cached, err := loadRelationsCache(cachePath); err == nil {
			log.Printf("Loaded relations from cache: %s", cachePath)
			return cached
		} else {
			log.Printf("Error decoding relations cache: %v", err)
		}
	}

	out := make(map[string][]string)
	data, err := ioutil.ReadFile(path)
	panicIfErr(err)
	lines := strings.Split(string(data), "\n")
	for _, line := range lines {
		if len(strings.TrimSpace(line)) == 0 {
			continue
		}
		fields := strings.Split(line, " ")
		if len(fields) < 3 {
			fmt.Printf("Error in qrels line: %v\n", line)
			continue
		}
		out[fields[0]] = append(out[fields[0]], fields[2])
	}

	if err := saveRelationsCache(cachePath, out); err != nil {
		log.Printf("Error saving relations cache: %v", err)
	} else {
		log.Printf("Saved relations to cache: %s", cachePath)
	}
	return out
}

// ---------------- Schema Function ----------------

func boolRef(b bool) *bool {
	return &b
}

func msmarcoSchema() *models.Class {
	return &models.Class{
		Class: "msmarco",
		Properties: []*models.Property{
			{
				IndexFilterable: boolRef(true),
				IndexSearchable: boolRef(true),
				Name:            "title",
				DataType:        []string{"text"},
				Tokenization:    Tokenization, // adjust as needed
			},
			{
				IndexFilterable: boolRef(true),
				IndexSearchable: boolRef(true),
				Name:         "text",
				DataType:     []string{"text"},
				Tokenization: Tokenization,
			},
			{
				IndexFilterable: boolRef(true),
				IndexSearchable: boolRef(true),
				Name:         "corpusid",
				DataType:     []string{"text"},
				Tokenization: Tokenization,
			},
		},
		ReplicationConfig: &models.ReplicationConfig{
			Factor: int64(Replicas),
		},
		Vectorizer: Vectorizer, // adjust as needed
	}
}
