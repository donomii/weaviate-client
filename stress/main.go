package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"

	weaviate "gitlab.com/donomii/weaviate-client"

	"math/rand"
	"time"
)

var (
	baseURL     string
	basePort    int
	numNodes    int
	currentNode int
)
var testclass string = "JeopardyQuestion"

const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 "

//json response:
/*
	[
    {
        "class": testclass,
        "creationTimeUnix": 1721998409360,
        "id": "861b8dd2-33b7-4356-9ebc-73c22ae1439f",
        "lastUpdateTimeUnix": 1721998409360,
        "properties": {
            "a_brand": "EgsyGlONILBEJrFz7IhTfYp7H0UstdMIM6ifQ2ZYYQu1Ei9flYMSWG4RnEy8uMATQLATtUJQCbaTgMSUjCqbqXtPtCmjpSEx8UqR",

        },
        "vector": []
}
		]
*/

type insertResponseStruct struct {
	Class              string                 `json:"class"`
	CreationTimeUnix   int64                  `json:"creationTimeUnix"`
	ID                 string                 `json:"id"`
	LastUpdateTimeUnix int64                  `json:"lastUpdateTimeUnix"`
	Properties         map[string]interface{} `json:"properties"`
	Vector             []float64              `json:"vector"`
}

func StartDeleteWorker(class string) chan string {
	// Create a channel to communicate with the worker
	deleteChan := make(chan string)

	// Start the worker
	go func() {
		for {
			// Wait for a message
			id := <-deleteChan
			// Delete the object
			err := weaviate.DeleteObject(class, id)
			if err != nil {
				fmt.Printf("Error deleting object: %v\n", err)
			} else {
				fmt.Printf("Deleted object with ID: %v\n", id)
			}
		}
	}()

	// Return the channel
	return deleteChan
}

func main() {
	var start, count, batchSize int
	flag.IntVar(&start, "start", 0, "start index")
	flag.IntVar(&count, "count", 1000, "number of objects to insert")
	flag.IntVar(&batchSize, "batch", 50, "batch size for insertion")
	flag.StringVar(&baseURL, "base-url", "http://localhost", "base URL for Weaviate nodes")
	flag.IntVar(&basePort, "base-port", 8080, "base port number for Weaviate nodes")
	flag.IntVar(&numNodes, "num-nodes", 1, "number of Weaviate nodes")
	flag.Parse()

	rand.Seed(time.Now().UnixNano())

	var propnames = []string{"a_brand"}//, "b_title", "c_retailer", "d_description", "retailer_parent_name", "product_url", "product_image_url", "additional_product_images", "retailer_display_name", "top_level_category", "category_tag", "language", "currency", "commission_threhsold", "gtin", "rpv_id", "generic_1", "generic_2", "generic_3", "generic_4"}

	ids := []string{}

   throttle := make(chan struct{}, 3)

	for i := start; i < start+count; i += batchSize {
		for i := 0; i < 1; i++ {
			class := fmt.Sprintf("%v%v", testclass, i)
			end := i + batchSize
			if end > start+count {
				end = start + count
			}

			maxPropCount := 0

			objs := []weaviate.ObjectInsert{}
			for j := start; j < end; j++ {
				jstr := randomString(1000000)
				countProps := 0
				props := map[string]interface{}{}
				for _, propname := range propnames {
					props[propname] = []byte(jstr)
					countProps++
				}
				if countProps > maxPropCount {
					maxPropCount = countProps
				}
				//Make a random vector
				randomVector := make([]float32, 300)
				for i := range randomVector {
					randomVector[i] = rand.Float32()
				}
				objs = append(objs, weaviate.ObjectInsert{
					Class:      class,
					Properties: props,
					Vector:    randomVector,
				})
			}



			go func() {
                throttle <- struct{}{}
                defer func() {
                    <-throttle
                }()
                insert := weaviate.ObjectsInsert{Objects: objs}
			fmt.Printf("Inserting %v objects with %v properties each into class %v\n", len(insert.Objects), maxPropCount, class)
				bodyBytes, err := insertObjectsWithRetry(insert)
				if err != nil {
					fmt.Printf("Error inserting objects: %v\n", err)
					return
				}

				if false {
					var response []insertResponseStruct
					err = json.Unmarshal(bodyBytes, &response)
					if err != nil {
						fmt.Printf("Error unmarshaling response: %v\n", err)
						return
					}

					for _, obj := range response {
						fmt.Printf("Inserted: %v into %v\n", obj.ID, testclass)
						ids = append(ids, obj.ID)
						go func(id string) {
							time.Sleep(20 * time.Second)
							err := deleteObjectWithRetry(class, id)
							if err != nil {
								fmt.Printf("\n\n")
								fmt.Printf("ERROR: Failed to delete object %s\n", id)
								fmt.Printf("ERROR: %v\n", err)
								fmt.Printf("\n\n")
								os.Exit(1)
							}
						}(obj.ID)
					}
				}
			}()
		}
	}

	// Wait for all objects to be inserted and deleted
    for len(throttle) > 0 {
        time.Sleep(1 * time.Second)
    }
}

func insertObjectsWithRetry(insert weaviate.ObjectsInsert) ([]byte, error) {
	var lastErr error
	for i := 0; i < numNodes; i++ {
		nodeURL := getNextNodeURL()
		bodyBytes, err := weaviate.InsertObjectsToNode(insert, nodeURL)
		if err == nil {
			return bodyBytes, nil
		}
		lastErr = err
		fmt.Printf("Failed to insert objects to %s: %v. Trying next node...\n", nodeURL, err)
	}
	return nil, fmt.Errorf("failed to insert objects after trying all nodes: %v", lastErr)
}

func deleteObjectWithRetry(class, id string) error {
	return nil
	var lastErr error
	for i := 0; i < numNodes; i++ {
		nodeURL := getNextNodeURL()
		err := weaviate.DeleteObjectFromNode(class, id, nodeURL)
		if err == nil {
			return nil
		}
		lastErr = err
		fmt.Printf("Failed to delete object from %s: %v. Trying next node...\n", nodeURL, err)
	}
	return fmt.Errorf("failed to delete object after trying all nodes: %v", lastErr)
}

func getNextNodeURL() string {
	currentNode = (currentNode + 1) % numNodes
	return fmt.Sprintf("%s:%d", baseURL, basePort+currentNode)
}

func randomString(length int) string {
	seededRand := rand.New(rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func randomObjs(start, count int) []map[string]interface{} {
	objs := []map[string]interface{}{}
	for i := start; i < start+count; i++ {
		obj := map[string]interface{}{}
		jstr := ""
		for j := 0; j < i; j++ {
			jstr += "a"
			obj[fmt.Sprintf("propname%v", j)] = jstr

			objs = append(objs, obj)

		}
	}
	return objs
}
