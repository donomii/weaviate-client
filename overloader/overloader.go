package main

//Steps to reproduce:
//
//  1. Delete the weaviate data directory, entirely (e.g. rm -r data)
//
// 2. Run this program with the following command line:
//    go run overloader.go -num-stressors 20000
//
// 3. Attempt a query for "fox"
//
// 4. Wait for the program to complete, or cancel it when you get bored.  A minute or two should be enough.
//
// 5. Wait until you see something like:
//    DEBU[0054] building bloom filter took 1.518875ms         action=lsm_precompute_disk_segment_build_bloom_filter_secondary class=Page index=page path=data/page_Gete4e2Uyy4M_lsm/objects/segment=
//DEBU[0054] building bloom filter took 1.35ms             action=lsm_precompute_disk_segment_build_bloom_filter_primary class=Page index=page path=data/page_Gete4e2Uyy4M_lsm/objects/segment-1=
//
// 6. Wait a bit longer?
//
// 7. Attempt a query for "fox" again.

import (
	"runtime"

	"flag"
	"fmt"
	"log"
	"math/rand"

	"os"
	"os/signal"

	"gitlab.com/donomii/weaviate-client"
)

var databaseSaverCh chan weaviate.ObjectInsert
var throttle chan bool
var stressors chan bool

func immediateInsertPage(class, url, title, content, source string) {

	throttle <- true
	defer func() { <-throttle }()
	//fmt.Printf("Enqueing page: %v, Database Q: %v/%v\n", url, len(databaseSaverCh), cap(databaseSaverCh))

	object := weaviate.ObjectInsert{

		Class: class,

		Properties: map[string]interface{}{
			"url":     url,
			"title":   title,
			"content": content,
			//"source": source,
		},
	}
	objList := weaviate.ObjectsInsert{
		Objects: []weaviate.ObjectInsert{
			object,
		},
	}

	err := weaviate.InsertObjects(objList)
	if err != nil {
		fmt.Println("Error inserting object: ", err)
		log.Println("Error inserting object: ", err)

	}
}

func main() {
	throttle = make(chan bool, 20)

	// Wait for interrupt signal to gracefully shutdown the server
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt, os.Kill)
	var err error
	//os.Mkdir("webcache.moss", 0755)
	//store, collection, err = moss.OpenStoreCollection("webcache.moss", moss.StoreOptions{}, moss.StorePersistOptions{})
	if err != nil {
		log.Fatal(err)
	}

	var numstressors int

	flag.StringVar(&weaviate.WeaviateUrl, "weaviate-url", weaviate.WeaviateUrl, "URL of weaviate instance.  Include the /v1/ at the end")
	flag.IntVar(&numstressors, "num-stressors", 1, "Number of stressors to run")
	flag.Parse()

	stressors = make(chan bool, numstressors)
	class := fmt.Sprintf("ClassA")

	for i := 0; i < numstressors; i++ {
		go startstress(i,class)
	}

	class = fmt.Sprintf("ClassB")

	//for i := 0; i < numstressors; i++ {
		//go startstress(i,class)
	//}

	<-quit
	log.Println("Shutdown Server ...")

}

func recur(class string, HopsRemaining int) {
	runtime.GC()
	if HopsRemaining < 1 {

		return
	}

	url := fmt.Sprintf("The quick brown fox jumped over the lazy dog %v", HopsRemaining)
	
	page := make([]byte, 4000)
	rand.Read(page)
	content := make([]byte, 4000)
	rand.Read(content)
	

	immediateInsertPage(class, string(url), string(page), string(content), string(content))

	recur(class, HopsRemaining - 1)

}

func startstress(n int, class string) {
	stressors <- true
	defer func() { <-stressors }()
	fmt.Printf("Starting %v\n", n)
	
	recur(class, 20000)
	fmt.Printf("Complete %v\n",n)

}
