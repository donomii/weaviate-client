package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"os/exec"
	//"net/http"
	"path/filepath"
	"strings"

	weaviate ".."

	"log"
	"unicode/utf8"
)

var throttle chan bool
var storeEntireFileData bool

func isTextFile(filepath string) bool {
	file, err := os.Open(filepath)
	if err != nil {
		return false
	}
	defer file.Close()

	reader := bufio.NewReader(file)

	// Check for byte order mark (BOM)
	b, err := reader.Peek(3)
	if err == nil && len(b) >= 3 && b[0] == 0xEF && b[1] == 0xBB && b[2] == 0xBF {
		return true // UTF-8 encoded text file with BOM
	}

	err = nil
	for err == nil {
		var line string
		line, err = reader.ReadString('\n')
		if !utf8.ValidString(line) {
			return false // binary file
		}
	}
	return true
}

func main() {
	throttle = make(chan bool, 5)
	flag.StringVar(&weaviate.WeaviateUrl, "weaviate-url", weaviate.WeaviateUrl, "URL of weaviate instance.  Include the /v1/ at the end")
	flag.BoolVar(&storeEntireFileData, "store-entire-file", storeEntireFileData, "Store the entire file in Weaviate.  Default is to store only the vector and url.")
	flag.Parse()

	// Parse command-line arguments
	if len(flag.Args()) != 1 {
		fmt.Println("Usage: files2weaviate  <directory>")
		os.Exit(1)
	}

	directory := flag.Args()[0]

	// Recursively traverse the directory
	filepath.Walk(directory, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return nil
		}
		if len(path) == 0 {
			return nil
		}
		if len(path) > 2000 {
			return nil
		}
		if info.IsDir() {
			return nil
		}

		if info.Mode().IsRegular() == false {
			return nil
		}

		fmt.Printf("Processing %s...", path)

		var content_bytes []byte
		// Check if the file is an epub
		switch {
		case strings.HasSuffix(strings.ToLower(path), ".epub"):
			fmt.Printf("epub...")
			// Use epub2txt to extract the text
			cmd := exec.Command("epub2txt", "-a", "-n", path)
			out, err := cmd.Output()
			if err != nil {
				log.Printf("Error extracting text from %s: %s", path, err)
			}
			content_bytes = out
		case strings.HasSuffix(strings.ToLower(path), ".pdf"):
			fmt.Printf("pdf...")
			//use pdftotext to extract the text
			cmd := exec.Command("pdftotext", path)
			out, err := cmd.Output()
			if err != nil {
				log.Printf("Error extracting text from %s: %s", path, err)
			}
			content_bytes = out
		case isTextFile(path):
			fmt.Printf("text...")
			content_bytes, err = os.ReadFile(path)
			if err != nil {
				log.Printf("Error reading text from %s: %s", path, err)
			}
		default:
			fmt.Printf("unknown, storing path only\n")
		}

		fmt.Printf("inserting...")

		var content = string(content_bytes)
		var contentChunks []string
		var objs []weaviate.ObjectInsert

		if !storeEntireFileData {
			//Calculate the vector
			vector, err := weaviate.Mod26Vector(path+" "+content)
			if err != nil {
				log.Printf("Error calculating vector for %s: %s", path, err)
				fmt.Printf("Error calculating vector for %s: %s\n", path, err)
				return nil
			}
			object := weaviate.ObjectInsert{
				Class: "Files",
				Properties: map[string]interface{}{
					"text": "",
					"path": path,
					"url":  "file://" + path,

				},
				Vector: vector,
			}
			objs = append(objs, object)

		} else {

			if len(content) < 20000000 {
				//Break content into chunks
				if len(content) > 1000000 {
					//Split into 1MB chunks
					for i := 0; i < len(content); i += 1000000 {
						if i+1000000 > len(content) {
							contentChunks = append(contentChunks, content[i:])
						} else {
							contentChunks = append(contentChunks, content[i:i+1000000])
						}
					}
				} else {
					contentChunks = append(contentChunks, content)
				}
			}


			for _, chunk := range contentChunks {
				object := weaviate.ObjectInsert{
					Class: "Files",
					Properties: map[string]interface{}{
						"text": string(chunk),
						"path": path,
						"url":  "file://" + path,
					},
				}
				objs = append(objs, object)
			}
		}

		insertobj := weaviate.ObjectsInsert{
			Objects: objs,
		}

		weaviate.InsertObjects(insertobj)
		if err != nil {
			log.Printf("Error submitting %s to Weaviate: %s", path, err)
			fmt.Printf("Error submitting %s to Weaviate: %s\n", path, err)
			return nil
		}

		//immediateInsertPage("file://"+path, info.Name(), string(content_bytes), string(content_bytes))

		fmt.Printf("submitted!\n")
		return nil
	})
	fmt.Printf("Submitted data to: %v\n", weaviate.WeaviateUrl)
}
