package main

import (
	"runtime"
	"runtime/debug"
	"strconv"

	//"github.com/couchbase/moss"
	"github.com/donomii/ensemblekv"

	"bytes"
	"encoding/json"
	"sync"

	"flag"
	"fmt"
	"regexp"

	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/jaytaylor/html2text"
	"golang.org/x/net/html"

	"net/url"

	"github.com/PuerkitoBio/purell"
	"github.com/donomii/goof"
	"github.com/recoilme/pudge"

	weaviate "gitlab.com/donomii/weaviate-client"

	//"github.com/boltdb/bolt"
)

var trace bool
var runStartTime time.Time
var countInserts int
var flagFinishedInserts bool
var ProgramWaitGroup sync.WaitGroup
var waiting = 0

func t(s string, args ...string) {
	if trace {
		fmt.Printf("Entering %v(%v)\n", s, strings.Join(args, ","))
	}
}

func l(s string, args ...string) {
	if trace {
		fmt.Printf("Leaving %v(%v)\n", s, strings.Join(args, ","))
	}
}

var GlobalDebug = false

func Debugf(format string, args ...interface{}) {
	if GlobalDebug {
		log.Printf(format, args...)
	}
}

type Config struct {
	BlockedDomains []string
	SiteRip        []string
}

var conf Config

var matchUrl = ""
var resume = false
var seenURL sync.Map
var databaseSaverCh chan weaviate.ObjectInsert
var throttle chan bool
var spiders chan bool
var searchwords []string
var banwords []string
var ignoreCache bool

var status struct {
	DatabaseQ int
	Inserting string
	FetchQ    int
	Fetching  string
}

func cleanURL(_url string) (string, error) {
	// Normalise URL, remove framents, etc
	_url = strings.Trim(_url, " ")
	_url = strings.Trim(_url, "\"")
	_url = strings.Trim(_url, "%22")
	_url = strings.Trim(_url, "\n")
	out, err := purell.NormalizeURLString(_url, purell.FlagsUsuallySafeGreedy|purell.FlagRemoveFragment)
	return out, err
}

type Objects struct {
	Objects []Object
}

type Object struct {
	Class      string                 "json:\"class\""
	Properties map[string]interface{} "json:\"properties\""
	Vector     []float32              "json:\"vector\""
}

func insertPage(url, title, content, source string) {
	t("insertPage", url, title)
	defer l("insertPage", url, title)
	throttle <- true
	defer func() { <-throttle }()

	fmt.Printf("Inserting page: %v\n", url)
	status.DatabaseQ = len(databaseSaverCh)
	status.Inserting = url
	ProgramWaitGroup.Add(1)
	waiting++
	object := weaviate.ObjectInsert{

		Class: "Page",

		Properties: map[string]interface{}{
			"url":     url,
			"title":   title,
			"content": content,
			//"source": source,
		},
	}

	if !storeEntireFileData {
		//Calculate the vector
		vector, err := weaviate.Mod26Vector(title + " " + url + " " + content)
		if err != nil {
			log.Printf("Error calculating vector for %s: %s", url, err)
			fmt.Printf("Error calculating vector for %s: %s\n", url, err)
			return
		}
		object.Vector = vector
		delete(object.Properties, "content")
	}

	databaseSaverCh <- object

}

func immediateInsertPageWeaviate(url, title, content, source string) {
	t("immediateInsertPage", url, title)
	defer l("immediateInsertPage", url, title)
	throttle <- true
	defer func() { <-throttle }()
	fmt.Printf("Inserting single page into weaviate: %v\n", url)
	status.DatabaseQ = len(databaseSaverCh)
	status.Inserting = url

	var objects []weaviate.ObjectInsert
	object := weaviate.ObjectInsert{

		Class: "Page",

		Properties: map[string]interface{}{
			"url":     url,
			"title":   title,
			"content": content,
			//"source": source,
		},
	}
	if !storeEntireFileData {
		//Calculate the vector
		vector, err := weaviate.Mod26Vector(title + " " + url + " " + content)
		if err != nil {
			log.Printf("Error calculating vector for %s: %s", url, err)
			fmt.Printf("Error calculating vector for %s: %s\n", url, err)
			return
		}
		object.Vector = vector
		delete(object.Properties, "content")
	}
	objects = append(objects, object)

	objList := weaviate.ObjectsInsert{
		Objects: objects,
	}

	countInserts++
	_, err := weaviate.InsertObjects(objList)
	if err != nil {
		fmt.Println("Error inserting object: ", err)
		log.Println("Error inserting object: ", err)

	}
}

func batchInsertPageWeaviate(url, title, content, source string) {
	t("batchInsertPage", url, title)
	defer l("batchInsertPage", url, title)
	//fmt.Printf("Batching page into weaviate: %v\n", url)
	status.DatabaseQ = len(databaseSaverCh)
	status.Inserting = url
	object := weaviate.ObjectInsert{

		Class: "Page",

		Properties: map[string]interface{}{
			"url":     url,
			"title":   title,
			"content": content,
			//"source": source,
		},
	}
	if !storeEntireFileData {
		//Calculate the vector
		vector, err := weaviate.Mod26Vector(title + " " + url + " " + content)
		if err != nil {
			log.Printf("Error calculating vector for %s: %s", url, err)
			fmt.Printf("Error calculating vector for %s: %s\n", url, err)
			return
		}
		object.Vector = vector
		delete(object.Properties, "content")
	}
	ProgramWaitGroup.Add(1)
	waiting++
	databaseSaverCh <- object
}

func batchManager() {
	t("batchManager")
	l("batchManager")
	defer func() {
		if err := recover(); err != nil {
			log.Println("panic occurred in manager:", err)
		}
	}()
	for {
		batchWorker()
	}
}

func batchWorker() {
	t("batchWorker")
	l("batchWorker")
	defer func() {
		if err := recover(); err != nil {
			log.Println("panic occurred in worker:", err)
		}
	}()
	//Read 100 objects from the channel, and insert them.  If more than 5 seconds pass without an object, insert what we have
	objects := []weaviate.ObjectInsert{}
	for {
		select {
		case object := <-databaseSaverCh:
			objects = append(objects, object)
			if len(objects) > 100 {
				fmt.Printf("objects per second: %v/%v = %v\n", countInserts, int(time.Since(runStartTime).Seconds()), countInserts/(1+int(time.Since(runStartTime).Seconds())))
				fmt.Println("Batch ready:", len(objects))
				fmt.Printf("Waiting: %v\n", waiting)
				break
			}
			continue
		case <-time.After(5 * time.Second):
			fmt.Printf("objects per second: %v/%v = %v\n", countInserts, int(time.Since(runStartTime).Seconds()), countInserts/(1+int(time.Since(runStartTime).Seconds())))
			fmt.Printf("Timeout exceeded, flushing remaining objects(%v)", len(objects))
			fmt.Printf("Waiting: %v\n", waiting)
			break
		}

		if len(objects) > 0 {
			flagFinishedInserts = false

			go func(objects []weaviate.ObjectInsert) {
				//fmt.Println("Inserting batch of objects:", len(objects))
				fmt.Print(".")
				countInserts += len(objects)
				objList := weaviate.ObjectsInsert{
					Objects: objects,
				}
				for i := 0; i < len(objects); i++ {
					ProgramWaitGroup.Done()
					waiting--
				}
				_, err := weaviate.InsertObjects(objList)

				if err != nil {
					fmt.Println("Error inserting object: ", err)
					log.Println("Error inserting object: ", err)

				}
			}(objects)
			objects = []weaviate.ObjectInsert{}
		} else {
			flagFinishedInserts = true
			fmt.Println("No objects to insert, set finished flags")
		}
	}
}

func statsPrinter() {
	for {
		printStatus()
		time.Sleep(10 * time.Second)

	}
}

var tkv *ensemblekv.TreeLSM
var storeEntireFileData = false
var startUrl string

func main() {
	Debugf("Starting ripper")
	runStartTime = time.Now()
	throttle = make(chan bool, 100)

	var err error

	seenURL = sync.Map{}
	var scanFile string        //Search a file for urls and load them as seeds
	var numSpiders int         //Number of parallel spiders
	var reloadSearchIndex bool //Don't access the web, just load everything from the cache into the search engine
	var siteRip bool
	var depth int //Depth to spider to
	var dumpPage string

	// Set (directories will be created)
	//pudge.Set("webcache.pudge", "Hello", "World")
	// flag.StringVar(&gopherType, "gopher_type", defaultGopher, usage)
	flag.StringVar(&matchUrl, "match-url", matchUrl, "URL to match")
	flag.StringVar(&weaviate.WeaviateUrl, "weaviate-url", weaviate.WeaviateUrl, "URL of weaviate instance.  Include the /v1/ at the end")
	flag.BoolVar(&resume, "resume", resume, "Use cache to resume spidering")
	flag.BoolVar(&reloadSearchIndex, "reload-search-index", reloadSearchIndex, "Reload the search index")
	flag.StringVar(&scanFile, "scan-file", scanFile, "File to scan for URLs")
	flag.BoolVar(&siteRip, "site-rip", siteRip, "Rip sites from config file")
	flag.IntVar(&numSpiders, "num-spiders", 10, "Number of parallel spiders to run")
	searchStr := flag.String("search", "", "Only follow pages containing these strings, command separated")
	banStr := flag.String("ban", "", "Do not follow pages containing these strings, comma separated")
	flag.BoolVar(&ignoreCache, "ignore-cache", false, "Ignore cache")
	flag.BoolVar(&trace, "trace", false, "Trace output")
	flag.BoolVar(&GlobalDebug, "debug", false, "Debug output")
	flag.IntVar(&depth, "depth", 2, "Depth to spider to")
	flag.StringVar(&dumpPage, "dump-page", "", "Dump page to stdout")
	flag.BoolVar(&storeEntireFileData, "store-entire-file", storeEntireFileData, "Store the entire file in Weaviate.  Default is to store only the vector and url.")
	flag.StringVar(&startUrl, "start-url", "", "Start URL")
	var listCache bool
	flag.BoolVar(&listCache, "list-cache", false, "List cache")
	flag.Parse()
	if startUrl != "" && scanFile != "" && !resume && !reloadSearchIndex && dumpPage == "" {
		fmt.Println("Specify an action: start-url, scan-file, resume or reload-search-index")
		return
	}
	fmt.Println("Parsed flags, starting ripper")

	/*
		Debugf("Opening bolt")
		db, err = bolt.Open("webcache.bolt", 0600, nil)
		if err != nil {
			log.Fatal(err)
		}
		defer db.Close()
	*/

	tkv, err = ensemblekv.NewTreeLSM("webcache.tkv", 1000000, ensemblekv.ExtentCreator)
	if err != nil {
		log.Fatal(err)
	}
	defer tkv.Close()

	/*
		Debugf("Creating bucket")
		db.Update(func(tx *bolt.Tx) error {
			_, err := tx.CreateBucket([]byte("MyBucket"))
			if err != nil {
				return fmt.Errorf("create bucket: %s", err)
			}
			return nil
		})
	*/

	databaseSaverCh = make(chan weaviate.ObjectInsert, 1000)
	go batchManager()

	if dumpPage != "" {
		mimetype, body, code, err, _ := loadPage(dumpPage)
		if err != nil {
			fmt.Printf("Error loading page: %v\n", err)
		}
		fmt.Printf("Mimetype: %v\n", mimetype)
		fmt.Printf("Code: %v\n", code)
		fmt.Printf("Body: %v\n", body)
		return
	}

	searchwords = strings.Split(*searchStr, ",")
	//Remove empties
	var newSearchWords []string
	for _, word := range searchwords {
		if word != "" {
			newSearchWords = append(newSearchWords, word)
		}
	}
	searchwords = newSearchWords
	fmt.Printf("Search words: %v\n", searchwords)
	banwords = strings.Split(*banStr, ",")
	//Remove empties
	var newBanWords []string
	for _, word := range banwords {
		if word != "" {
			newBanWords = append(newBanWords, word)
		}
	}
	banwords = newBanWords
	fmt.Printf("Ban words: %v\n", banwords)

	spiders = make(chan bool, numSpiders)

	if !goof.Exists("ripper.conf") {
		fmt.Printf("No config file found, creating one\n")
		conf.BlockedDomains = []string{}
		confData, err := json.Marshal(conf)
		if err != nil {
			fmt.Printf("Error creating config file: %v\n", err)
		}
		err = ioutil.WriteFile("ripper.conf", confData, 0644)
		if err != nil {
			fmt.Printf("Error creating config file: %v\n", err)
		}
	}

	confData, err := ioutil.ReadFile("ripper.conf")
	if err != nil {
		fmt.Printf("Error reading config file: %v\n", err)
	}
	err = json.Unmarshal(confData, &conf)
	if err != nil {
		fmt.Printf("Error parsing config file: %v\n", err)
	}

	if siteRip && conf.SiteRip != nil {
		for _, url := range conf.SiteRip {
			url, err := cleanURL(url)
			if err != nil {
				fmt.Printf("Error parsing start URL: %v\n", err)
				continue
			}
			seed := true //All urls from a user file are seeds
			ProgramWaitGroup.Add(1)
			waiting++
			go StartSpider(url, seed, 100, url)
		}
	}

	if startUrl != "" {
		url, err := cleanURL(startUrl)
		if err != nil {
			fmt.Printf("Error parsing start URL: %v\n", err)

		} else {
			ProgramWaitGroup.Add(1)
			waiting++
			fmt.Println("Starting spider for", url)
			go StartSpider(url, true, depth, "")
		}
	}
	if scanFile != "" {
		buf, err := ioutil.ReadFile(scanFile)
		if err != nil {
			fmt.Printf("Error reading file: %v\n", err)
		}
		//Use regular expressions to pull out URLs
		regex := regexp.MustCompile(`(https?://[^\s]+)`)
		matches := regex.FindAllString(string(buf), -1)

		for _, url := range matches {
			url, err := cleanURL(url)
			if err != nil {
				fmt.Printf("Error parsing start URL: %v\n", err)
				continue
			}
			seed := true //All urls from a user file are seeds
			ProgramWaitGroup.Add(1)
			waiting++
			go StartSpider(url, seed, depth, "")
		}
	}
	/*
	   	if reloadSearchIndex {
	   		fmt.Println("Reloading search index")

	   		db.View(func(tx *bolt.Tx) error {
	   			// Assume bucket exists and has keys
	   			b := tx.Bucket([]byte("MyBucket"))

	   			c := b.Cursor()

	   			for k, _ := c.First(); k != nil; k, _ = c.Next() {
	   				if bytes.HasPrefix(k, []byte("http")) {

	   					data, err := getFromCache(string(k))
	   					if err != nil {
	   						fmt.Printf("Error getting page: %v\n", err)
	   						continue
	   					}
	   					p := Page{}
	   					err = json.Unmarshal(data, &p)
	   					if err != nil {
	   						fmt.Printf("Error unmarshalling page: %v\n", err)
	   						continue
	   					}
	   					if p.Code > 199 && p.Code < 300 {
	   						if strings.HasPrefix(p.Mimetype, "text") {
	   							batchInsertPageWeaviate(string(k), p.Title, string(p.Content), string(p.Content))
	   						}
	   					} else {
	   						fmt.Printf("Skipping %v because code is  %v\n", string(k), p.Code)
	   					}

	   				}
	   			}
	   			return nil

	   		})
	   }
	*/

	tkv.MapFunc(func(key, value []byte) error {
		if bytes.HasPrefix(key, []byte("http")) {

			p := Page{}
			err = json.Unmarshal(value, &p)
			if err != nil {
				fmt.Printf("Error unmarshalling page: %v\n", err)
				return nil
			}
			if p.Code > 199 && p.Code < 300 {
				if strings.HasPrefix(p.Mimetype, "text") {
					batchInsertPageWeaviate(string(key), p.Title, string(p.Content), string(p.Content))
				}
			} else {
				fmt.Printf("Skipping %v because code is  %v\n", string(key), p.Code)
			}

		}
		return nil
	})

	if listCache {
		tkv.MapFunc(func(key, value []byte) error {
			if bytes.HasPrefix(key, []byte("http")) {
				fmt.Printf("URL: %v\n", string(key))
			}
			return nil
		})
	}

	if resume {
		fmt.Println("Starting searches from cached data")
		keys := [][]byte{}
		/*
			db.View(func(tx *bolt.Tx) error {
				// Assume bucket exists and has keys
				b := tx.Bucket([]byte("MyBucket"))

				c := b.Cursor()

				for k, _ := c.First(); k != nil; k, _ = c.Next() {
					if bytes.HasPrefix(k, []byte("http")) {
						keys = append(keys, k)
					}
				}

				return nil
			})
		*/

		tkv.MapFunc(func(key, value []byte) error {
			if bytes.HasPrefix(key, []byte("http")) {
				keys = append(keys, key)
			}
			return nil
		})

		fmt.Printf("Found %v keys\n", len(keys))

		//If we launch directly from the keys loop, we end up in an infinite loop, as we keep loading new keys from the cache and following them
		for _, k := range keys {
			url := string(k)
			data, err := getFromCache(url)
			if err != nil {
				fmt.Printf("Error getting page: %v\n", err)
				continue
			}
			p := Page{}
			err = json.Unmarshal(data, &p)
			if err != nil {
				fmt.Printf("Error unmarshalling page: %v\n", err)
				continue
			}
			seed := p.Seed
			ProgramWaitGroup.Add(1)
			waiting++

			go StartSpider(url, seed, depth, "")
		}
	}
	Debugf("Main thread waiting on waitgroup")
	fmt.Println("Main thread waiting on waitgroup")
	ProgramWaitGroup.Wait()

	for {
		fmt.Println("Waiting for finish flag")
		time.Sleep(1 * time.Second)
		if flagFinishedInserts {
			break
		}
	}
	log.Println("Shutdown Server ...")
	if err := pudge.CloseAll(); err != nil {
		log.Println("Pudge Shutdown err:", err)
	}
}

func saveToCache(url string, body []byte) {
	t("saveToCache", url)
	defer l("saveToCache", url)
	/*	batch, _ := collection.NewBatch(0, 0)
		defer batch.Close()

		batch.Set([]byte(url), []byte(body))

		collection.ExecuteBatch(batch, moss.WriteOptions{})
	*/
	/*
		db.Update(func(tx *bolt.Tx) error {
			b := tx.Bucket([]byte("MyBucket"))
			err := b.Put([]byte(url), []byte(body))
			return err
		})
	*/

	tkv.Put([]byte(url), []byte(body))

	//pudge.Set("webcache.pudge", url, body)
}

func getFromCache(url string) ([]byte, error) {
	t("getFromCache", url)
	defer l("getFromCache", url)
	var val []byte
	/*
		db.View(func(tx *bolt.Tx) error {
			b := tx.Bucket([]byte("MyBucket"))
			val = b.Get([]byte(url))

			return nil
		})
	*/

	val, err := tkv.Get([]byte(url))

	if err != nil {
		return nil, err
	}

	return bytes.Clone(val), nil
	/*
		ropts := moss.ReadOptions{}

		// A Get can also be issued directly against the collection
		val1, err := collection.Get([]byte(url), ropts)

		return bytes.Clone(val1), err
	*/
	/*
		var body []byte
		err = pudge.Get("webcache.pudge", url, &body)
		return bytes.Clone(body), err
	*/
}

func databaseSaverWorkerManager(databaseSaverCh chan weaviate.ObjectInsert) {
	t("databaseSaverWorkerManager")
	l("databaseSaverWorkerManager")
	defer func() {
		if err := recover(); err != nil {
			log.Println("panic occurred in manager:", err)
		}
	}()
	for {
		databaseSaverWorker(databaseSaverCh)
	}
}

func databaseSaverWorker(databaseSaverCh chan weaviate.ObjectInsert) {
	t("databaseSaverWorker")
	defer l("databaseSaverWorker")
	defer func() {
		if err := recover(); err != nil {
			log.Println("panic occurred in worker:", err)
		}
	}()
	for {

		object := <-databaseSaverCh

		//fmt.Printf("Received object to insert: %v, Database Q: %v/%v\n", object.Properties["url"], len(ch), cap(ch))
		objList := weaviate.ObjectsInsert{
			Objects: []weaviate.ObjectInsert{
				object,
			},
		}

		countInserts += len(objList.Objects)
		for i := 0; i < len(objList.Objects); i++ {
			ProgramWaitGroup.Done()
			waiting--
		}
		_, err := weaviate.InsertObjects(objList)

		if err != nil {
			fmt.Println("Error inserting object: ", err)
			log.Println("Error inserting object: ", err)

		}
	}
}

func processPage(strUrl, body []byte) []string {
	t("processPage", string(strUrl))
	defer l("processPage", string(strUrl))
	defer func() {
		if err := recover(); err != nil {
			log.Println("panic occurred in process page:", err)
			//Print stack dump
			debug.PrintStack()
		}
	}()

	if len(searchwords) > 0 && !inList(string(body), searchwords) {
		fmt.Printf("Skipping %v because it does not contain search words\n", string(strUrl))
		return []string{}
	}

	if len(banwords) > 0 && inList(string(body), banwords) {
		fmt.Printf("Skipping %v because it contains banned words\n", string(strUrl))
		return []string{}
	}

	urls := []string{}

	aUrl, err := url.Parse(string(strUrl))

	var (
		anchorTag = []byte{'a'}
		hrefTag   = []byte("href")
		httpTag   = []byte("http")
		imgTag    = []byte("img")
	)

	bodyR := bytes.NewReader(body)
	// defer bodyR.Close()

	buf, err := ioutil.ReadAll(bodyR)
	if err != nil {
		log.Println(err)
		return []string{}
	}
	// ioutil.WriteFile(path, buf, 0644)
	//plain := html2text.HTML2Text(string(buf))
	//fmt.Println("Converting HTML to text")
	plain, err := html2text.FromString(string(buf), html2text.Options{})
	if err != nil {
		fmt.Println(err)
		return []string{}
	}
	//fmt.Printf("Enqueing into local Q: %v\n", aUrl.String())
	if len(plain) > 100 {
		fmt.Printf("Inserting into search engine %v\n", aUrl.String())
		immediateInsertPageWeaviate(aUrl.String(), aUrl.String(), plain, string(buf)) //FIXME extract title?

		bodyR = bytes.NewReader(buf)
		tkzer := html.NewTokenizer(bodyR)

		//fmt.Printf("Processing for urls: %v\n", aUrl.String())
		for tk := tkzer.Next(); tk != html.ErrorToken; tk = tkzer.Next() {
			// log.Println(tkzer.TagName())
			//fmt.Print(".")
			switch tk {
			case html.ErrorToken:
				// HANDLE ERROR
				return []string{}

			case html.StartTagToken:
				tag, hasAttr := tkzer.TagName()
				if hasAttr && bytes.Equal(imgTag, tag) {
					// key, val, _ := tkzer.TagAttr()
					// fmt.Printf("%s, %s\n", key, val)
				}
				if hasAttr && bytes.Equal(anchorTag, tag) { // a
					// HANDLE ANCHOR
					key, val, _ := tkzer.TagAttr()

					if bytes.Equal(hrefTag, key) { // href, http(s)
						// HREF TAG
						//fmt.Printf("    %s, %s\n", key, val)
						if bytes.HasPrefix(val, httpTag) {
							// Filter here?
							urls = append(urls, fmt.Sprintf("%s", val))

						} else {
							if err == nil {
								//urls = append(urls, combineUrlAndPath(aUrl.String(), string(val)))
								urls = append(urls, CombinationCombineUrlAndPath(aUrl.String(), string(val))...)

							} else {
								fmt.Printf("In page %v, cannot parse: %s\n", aUrl, err)
							}
						}
					}

				}
			}

		}
	}

	bodyR = nil
	//Copy all urls so they aren't holding the original document open
	var outurls []string
	for _, url := range urls {
		newUrl := make([]byte, len(url))
		copy(newUrl, url)
		outurls = append(outurls, string(newUrl))
	}
	return outurls
}

func combineUrlAndPath(base, path string) string {
	t("combineUrlAndPath", base, path)
	defer l("combineUrlAndPath", base, path)
	if strings.HasPrefix(path, "mailto:") {
		return base
	}
	if strings.HasPrefix(path, "javascript:") {
		return base
	}
	if strings.HasPrefix(path, "http:") {
		return path
	}
	if strings.HasPrefix(path, "https:") {
		return path
	}

	_url, err := url.Parse(base)
	if err != nil {
		fmt.Printf("Error parsing URL: %v\n", err)
		return ""
	}

	if strings.HasPrefix(path, "/") {
		//Absolute path
		_url.Path = path
		return _url.String()
	}

	//Trim fragment from path
	if strings.Contains(path, "#") {

		path = path[:strings.Index(path, "#")]
	}

	if len(path) == 0 {
		return base
	}

	//Relative path

	//get base path
	basePath := _url.Path
	if strings.HasSuffix(basePath, "/") {
		//Basepath is a directory

		newPath := AbsoluteAndRelativePaths(basePath, path)

		newUrl := _url.JoinPath(newPath).String()
		fmt.Printf("combining urls: %v, (%v,%v) into %v\n", base, basePath, string(path), newUrl)
		return newUrl
	}
	//Basepath is a file

	//Trim basepath to last /
	lastSlash := strings.LastIndex(basePath, "/")
	if lastSlash > -1 {
		basePath = basePath[:lastSlash]
		_url.Path = basePath
	}
	newPath := AbsoluteAndRelativePaths(basePath, path)
	newUrl := _url.JoinPath(newPath).String()
	fmt.Printf("combining urls: %v, (%v,%v) into %v\n", base, basePath, string(path), newUrl)
	return newUrl

}

func CombinationCombineBaseAndPath(base, path string) []string {
	if strings.HasPrefix(path, "mailto:") {
		return []string{path}
	}
	if strings.HasPrefix(path, "javascript:") {
		return []string{path}
	}
	if strings.HasPrefix(path, "http:") {
		return []string{path}
	}
	if strings.HasPrefix(path, "https:") {
		return []string{path}
	}

	//If the path contains :// then it is a full url, we can return it
	if strings.Contains(path, "://") {
		return []string{path}
	}

	//Trim fragment from path
	if strings.Contains(path, "#") {

		path = path[:strings.Index(path, "#")]
	}

	//Trim fragment from base
	if strings.Contains(base, "#") {

		base = base[:strings.Index(base, "#")]
	}

	//Trim query from base
	if strings.Contains(base, "?") {

		base = base[:strings.Index(base, "?")]
	}

	if len(path) == 0 {
		return []string{base}
	}

	if len(base) == 0 {
		return []string{path}
	}

	baseDir := ""
	//If the base does not end with /, then trim the last part, up to a /
	if !strings.HasSuffix(base, "/") {
		lastSlash := strings.LastIndex(base, "/")
		if lastSlash > -1 {
			baseDir = base[:lastSlash]
		}
	}

	hostPart := ""
	//Cut the string at the thrid /, if it exists
	if strings.Count(base, "/") > 2 {
		hostPart = base[:strings.Index(base[8:], "/")+8]
	} else {
		hostPart = base
	}

	if strings.HasPrefix(path, "/") {
		//Absolute path
		outUrls := []string{}
		outUrls = append(outUrls, fmt.Sprintf("%v/%v", hostPart, path))
		return outUrls
	}

	outUrls := []string{}

	outUrls = append(outUrls, AbsoluteAndRelativePaths(base, path))
	outUrls = append(outUrls, AbsoluteAndRelativePaths(base+"/", path))
	outUrls = append(outUrls, AbsoluteAndRelativePaths(baseDir, path))
	outUrls = append(outUrls, AbsoluteAndRelativePaths(baseDir+"/", path))

	return outUrls
}

func CombinationCombineUrlAndPath(aUrl, path string) []string {
	t("CombinationCombineUrlAndPath", aUrl, path)
	defer l("CombinationCombineUrlAndPath", aUrl, path)
	//Get path from url
	_url, err := url.Parse(aUrl)
	if err != nil {
		fmt.Printf("Error parsing URL: %v\n", err)
		return []string{}
	}
	base := _url.Path
	paths := CombinationCombineBaseAndPath(base, path)
	outUrls := []string{}
	for _, path := range paths {
		_url.Path = path
		newUrl := strings.Clone(_url.String())
		outUrls = append(outUrls, newUrl)
		//fmt.Println("newUrl: ", newUrl)
	}
	return outUrls
}

func AbsoluteAndRelativePaths(base, path string) string {
	t("AbsoluteAndRelativePaths", base, path)
	defer l("AbsoluteAndRelativePaths", base, path)
	//Break paths into components
	baseParts := strings.Split(base, "/")
	pathParts := strings.Split(path, "/")
	//fmt.Printf("baseParts: %v\n", baseParts)
	//fmt.Printf("pathParts: %v\n", pathParts)
	//Remove empty parts
	var newParts []string
	for _, part := range baseParts {
		if part != "" {
			newParts = append(newParts, part)
		}
	}

	for _, part := range pathParts {
		if part == ".." {
			if len(newParts) == 0 {
				fmt.Printf("Error combining paths: %v, %v\n", base, path)
				//goof.Panicf("Error combining paths: %v, %v\n", base, path)
				return ""
			}
			//Remove last part
			newParts = newParts[:len(newParts)-1]
		} else {
			newParts = append(newParts, part)
		}
	}

	//Remove // from paths
	var newParts2 []string
	for _, part := range newParts {
		if part != "" {
			newParts2 = append(newParts2, part)
		}
	}
	newParts = newParts2

	//fmt.Printf("newParts: %v\n", newParts)
	return strings.Join(newParts, "/")
}

func printStatus() {
	fmt.Printf(`
------------------------------------------------------
+ Fetching: %v, %v      
+ Inserting: %v, %v   
+ Throttle: %v/%v
+-----------------------------------------------------
`, status.FetchQ, status.Fetching, status.DatabaseQ, status.Inserting, len(throttle), cap(throttle))
}

func inList(a string, list []string) bool {
	if len(list) == 0 {
		return false
	}
	for _, listeElem := range list {
		if listeElem == "" {
			continue
		}
		//fmt.Printf("Checking %v against %v\n", a, listeElem)
		if strings.Contains(strings.ToLower(a), strings.ToLower(listeElem)) {
			return true
		}
	}
	return false
}

func SimpleHttpGet(url string, timeout int) (body []byte, mimetype string, redirect string, code int) {
	t("SimpleHttpGet", url, fmt.Sprintf("%v", timeout))
	defer l("SimpleHttpGet", url, fmt.Sprintf("%v", timeout))
	throttle <- true
	defer func() {
		//fmt.Printf("Clearing throttle: %v/%v\n", len(throttle), cap(throttle))
		<-throttle
	}()

	time.Sleep(5 * time.Second)
	status.Fetching = url
	client := http.Client{
		Timeout: time.Duration(timeout) * time.Second,
	}
	req, err := http.NewRequest("GET", url, nil)
	resp, err := client.Do(req)
	if resp != nil && resp.Body != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		fmt.Println("Error fetching URL: ", err)
		return nil, "", "", -1
	}

	//r := http.MaxBytesReader(nil, resp.Body, 10*1024*1024)
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, "", "", 501 //FIXME maybe return partial content?
	}
	redirect = resp.Header.Get("Location")
	return bytes.Clone(body), resp.Header.Get("Content-Type"), redirect, resp.StatusCode
}

type Page struct {
	Url         string
	Title       string
	Content     []byte
	Mimetype    string
	Code        int
	TimeFetched time.Time
	Seed        bool
}

func storePage(url, title, mimetype string, content []byte, code int, seed bool) {
	t("storePage", string(url), string(mimetype))
	defer l("storePage", string(url), string(mimetype))

	p := Page{
		Url:         url,
		Title:       title,
		Content:     content,
		Mimetype:    mimetype,
		Code:        code,
		TimeFetched: time.Now(),
	}

	pageData, err := json.Marshal(p)
	if err != nil {
		fmt.Printf("Error marshalling page: %v\n", err)
		return
	}

	saveToCache(string(url), pageData)

}

func loadPage(url string) (mimetype string, content []byte, code int, err error, cache bool) {
	t("loadPage", url)
	defer l("loadPage", url)

	pageData := []byte{}
	pageData, err = getFromCache(url)
	if err != nil {
		return "", nil, -1, err, false
	}

	p := Page{}
	err = json.Unmarshal(pageData, &p)
	if err != nil {
		//fmt.Printf("Error unmarshalling page: %v\n", err)

		page, err := getFromCache("url:" + url)
		if err != nil {
			return "", nil, -1, err, false
		}
		mimetype_b, err := getFromCache("mimetype:" + url)
		if err != nil {
			return "", nil, -1, err, false
		}
		code_b, err := getFromCache("code:" + url)
		if err != nil {
			return "", nil, -1, err, false
		}
		code, _ = strconv.Atoi(string(code_b))
		return string(mimetype_b), page, code, nil, false
	}

	return p.Mimetype, p.Content, p.Code, nil, true

}

func GetAndProcessPage(url string, HopsRemaining int, seed bool) ([]string, string, int) {
	t("GetAndProcessPage", url, fmt.Sprintf("%v", HopsRemaining))
	defer l("GetAndProcessPage", url, fmt.Sprintf("%v", HopsRemaining))

	if matchUrl != "" && !strings.Contains(url, matchUrl) {
		return []string{}, "", -1
	}

	//fmt.Printf("Spidering %v, %v hops remaining\n", url, HopsRemaining)
	var cachedBody []byte

	mimetype_b, cachedBody, code, err, cached := loadPage(url)

	if (err == nil && cachedBody != nil && mimetype_b != "" && code < 500) && !ignoreCache && cached {
		fmt.Printf("Old: %v from cache\n", url)
		//FIXME store and check mimetype
		if strings.HasPrefix(string(mimetype_b), "text") {
			urls := processPage([]byte(url), cachedBody)
			return urls, string(mimetype_b), code
		} else {
			return []string{}, string(mimetype_b), code
		}

	} else {
		//fmt.Printf("Not found in cache: %v because: %v\n", url, err)
	}

	fmt.Printf("New: %v\n", url)
	body, mimetype, redirect, code := SimpleHttpGet(url, 5)
	//Follow redirects
	if code == 301 || code == 302 {

		body, mimetype, redirect, code = SimpleHttpGet(redirect, 10)
		fmt.Printf("Following redirect: from %v to %v\n", url, redirect)
		url = redirect
	}
	if code < 500 && code > 0 {
		fmt.Printf("Caching %v\n", url)
		storePage(url, url, mimetype, body, code, seed)

		if strings.HasPrefix(mimetype, "text") {
			urls := processPage([]byte(url), body)
			return urls, mimetype, code
		} else {
			return []string{}, mimetype, code
		}
	} else {
		fmt.Printf("Not caching and processing %v because code is %v\n", url, code)
	}
	return []string{}, "fail/fail", -1
}

func LimitedSpider(startUrl string, HopsRemaining int, seed bool, limitUrl string) {
	if limitUrl != "" && !strings.HasPrefix(startUrl, limitUrl) {
		fmt.Printf("Leaving early from LimitedSpider(%v) because it does not match limitUrl %v\n", startUrl, limitUrl)
		return
	}
	runtime.GC()
	t("LimitedSpider", startUrl, fmt.Sprintf("%v", HopsRemaining))
	defer l("LimitedSpider", startUrl, fmt.Sprintf("%v", HopsRemaining))
	if HopsRemaining < 0 {
		fmt.Printf("Leaving early from LimitedSpider(%v) because hops remaining is %v\n", startUrl, HopsRemaining)
		return
	}
	_, ok := seenURL.Load(startUrl)
	if !ok {
		seenURL.Store(startUrl, true)
	} else {
		//fmt.Printf("Seen: %v\n", startUrl)
		return
	}

	startUrl, err := cleanURL(startUrl)
	if err != nil {
		fmt.Printf("Error cleaning URL: %v\n", err)
		return
	}

	url, err := url.Parse(startUrl)
	if err != nil {
		fmt.Printf("Error parsing URL: %v\n", err)
		return
	}

	if inList(url.Hostname(), conf.BlockedDomains) {
		fmt.Printf("Blocked domain: %v\n", url.String())
		return
	}

	urls, mimetype, _ := GetAndProcessPage(startUrl, HopsRemaining, seed)
	//fmt.Println("code: ", code)

	if strings.HasPrefix(mimetype, "text/html") {

		for _, url := range urls {
			if HopsRemaining > 0 {
				//fmt.Printf("From parsed page: %v\n", url)
				data, err := getFromCache(url)
				if err != nil {

					LimitedSpider(url, HopsRemaining-1, false, "")
				} else {
					p := Page{}
					err = json.Unmarshal(data, &p)
					if err != nil {
						LimitedSpider(url, HopsRemaining-1, false, "")
					} else {
						LimitedSpider(url, HopsRemaining-1, p.Seed, "")
					}

				}
			}
		}
	}

}

func StartSpider(aUrl string, seed bool, depth int, limitUrl string) {

	spiders <- true
	defer func() { <-spiders }()
	t("StartSpider", aUrl)
	defer l("StartSpider", aUrl)
	fmt.Printf("Starting spider for %v\n", aUrl)
	LimitedSpider(aUrl, depth, seed, limitUrl)
	fmt.Printf("Spider complete for %v\n", aUrl)
	ProgramWaitGroup.Done()

}
