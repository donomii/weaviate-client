#!/bin/sh

echo "Killing servers"
pkill -f weaviate-server
echo "Deleting backups"
rm -r backups-weaviate*
echo "Deleting data"
rm -r data*
echo "Deleting logs"
rm -r server_*

#echo "Starting new servers"
#bash startmany.sh

sync

echo "Weaviate cluster has been reset"
