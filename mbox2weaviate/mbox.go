package main

import mbox "github.com/emersion/go-mbox"
import "os"
import "io"
import "fmt"
import "net/mail"
import weaviate "gitlab.com/donomii/weaviate-client"
import "flag"

func main() {

	flag.StringVar(&weaviate.WeaviateUrl, "weaviate-url", weaviate.WeaviateUrl, "URL of weaviate instance.  Include the /v1/ at the end")
	flag.Parse()

	// Parse command-line arguments
	if len(flag.Args()) != 1 {
		fmt.Println("Usage: mbox2weaviate  <inbox.mbox>")
		os.Exit(1)
	}

	file := flag.Args()[0]


	f, err := os.Open(file)
	if err != nil {
		fmt.Print("Oops, something went wrong!", err)
		return
	}
	defer f.Close()

	r := io.Reader(f)
	mr := mbox.NewReader(r)
	for {
		r, err := mr.NextMessage()
		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Print("Oops, something went wrong!", err)
			return
		}

		msg, err := mail.ReadMessage(r)
		if err != nil {
			fmt.Print("Oops, something went wrong!", err)
			return
		}

		fmt.Printf("Message from %v\n", msg.Header.Get("From"))
		bodyReader := msg.Body
		body, _ := io.ReadAll(bodyReader)


			object := weaviate.ObjectInsert{
				Class: "Email",
				Properties: map[string]interface{}{
					"to":   msg.Header.Get("To"),
					"from": msg.Header.Get("From"),
					"subject": msg.Header.Get("Subject"),
					"date": msg.Header.Get("Date"),
					"messageid": msg.Header.Get("Message-Id"),
					"contenttype": msg.Header.Get("Content-Type"),
					"body": string(body),
				},
			}

//			fmt.Printf("Inserting %+v\n", object)



		insertobj := weaviate.ObjectsInsert{
			Objects: []weaviate.ObjectInsert{object},
		}

		_, err = weaviate.InsertObjects(insertobj)
		if err != nil {

			fmt.Printf("Error submitting to Weaviate: %s\n", err)

		}
	}
}
