#!/usr/bin/env bash

NUM_SERVERS=4
MAX_RETRIES=1000

# export FORCE_FULL_REPLICAS_SEARCH=true
export ASYNC_INDEXING=false
export TOKENIZERS=false
export ENABLE_TOKENIZER_KAGOME_JA=$TOKENIZERS
export ENABLE_TOKENIZER_KAGOME_KR=$TOKENIZERS
export ENABLE_TOKENIZER_GSE=$TOKENIZERS

# Base ports (following example config)
BASE_HTTP_PORT=8080
BASE_GRPC_PORT=50051
BASE_RAFT_PORT=8300  # <- Only this goes in RAFT_JOIN
BASE_RAFT_INTERNAL_RPC_PORT=8301  # Internal RPC port
BASE_GOSSIP_PORT=7100  # <- Moves up by 4 each time to avoid conflicts
BASE_MONITORING_PORT=2112

# Generate Raft join list using correct ports
RAFT_JOIN_LIST=""
for i in $(seq 0 $((NUM_SERVERS - 1))); do
  RAFT_PORT=$((BASE_RAFT_PORT + (i * 2)))
  if [[ -z "$RAFT_JOIN_LIST" ]]; then
    RAFT_JOIN_LIST="weaviate-$i:$RAFT_PORT"
  else
    RAFT_JOIN_LIST="$RAFT_JOIN_LIST,weaviate-$i:$RAFT_PORT"
  fi
done

echo "Raft join list: $RAFT_JOIN_LIST"

find_available_port() {
  local port=$1
  while true; do
    # Try to bind a test process to the port
    (echo > /dev/tcp/127.0.0.1/$port) &>/dev/null && {
      ((port++)) # Increment and try the next port
    } || {
      echo "$port" # Return only the valid port number
      return
    }
  done
}

start_weaviate() {
  local i=$1
  local HOSTNAME="weaviate-$i"

  local HTTP_PORT=$(find_available_port $((BASE_HTTP_PORT + i)))
  local GRPC_PORT=$(find_available_port $((BASE_GRPC_PORT + i)))
  local RAFT_PORT=$((BASE_RAFT_PORT + (i * 2)))  # <- Correct Raft port
  local RAFT_INTERNAL_RPC_PORT=$((RAFT_PORT + 1))  # <- Internal RPC must be +1
  local GOSSIP_PORT=$((BASE_GOSSIP_PORT + (i * 4)))  # <- Corrected gossip offset
  local CLUSTER_DATA_PORT=$((GOSSIP_PORT + 1))  # <- Always +1 to avoid conflicts
  local MONITORING_PORT=$((BASE_MONITORING_PORT + i))

  local PERSISTENCE_DATA_PATH="data_$i"
  local BACKUP_PATH="${PWD}/backups-weaviate-$i"
  mkdir -p "$PERSISTENCE_DATA_PATH" "$BACKUP_PATH"

  echo "Starting Weaviate server $i:"
  echo "  HTTP port                 = $HTTP_PORT (Dynamic)"
  echo "  GRPC_PORT                 = $GRPC_PORT"
  echo "  RAFT_PORT                 = $RAFT_PORT"
  echo "  RAFT_INTERNAL_RPC_PORT    = $RAFT_INTERNAL_RPC_PORT"
  echo "  CLUSTER_GOSSIP_BIND_PORT  = $GOSSIP_PORT"
  echo "  CLUSTER_DATA_BIND_PORT    = $CLUSTER_DATA_PORT"
  echo "  CLUSTER_JOIN              = localhost:$BASE_GOSSIP_PORT"
  echo "  RAFT_JOIN_LIST            = $RAFT_JOIN_LIST"
  echo "  PERSISTENCE_DATA_PATH     = $PERSISTENCE_DATA_PATH"
  echo "  BACKUP_FILESYSTEM_PATH    = $BACKUP_PATH"
  echo "  PROMETHEUS_MONITORING_PORT = $MONITORING_PORT"

  # Run the server in the dedicated data folder to keep files separate
  (
    cd "$PERSISTENCE_DATA_PATH" || exit
    env \
      GRPC_PORT="$GRPC_PORT" \
      CLUSTER_HOSTNAME="$HOSTNAME" \
      CLUSTER_IN_LOCALHOST=true \
      CLUSTER_GOSSIP_BIND_PORT="$GOSSIP_PORT" \
      CLUSTER_DATA_BIND_PORT="$CLUSTER_DATA_PORT" \
      CLUSTER_JOIN="localhost:$BASE_GOSSIP_PORT" \
      PERSISTENCE_DATA_PATH="$PERSISTENCE_DATA_PATH" \
      BACKUP_FILESYSTEM_PATH="$BACKUP_PATH" \
      PROMETHEUS_MONITORING_PORT="$MONITORING_PORT" \
      RAFT_PORT="$RAFT_PORT" \
      RAFT_INTERNAL_RPC_PORT="$RAFT_INTERNAL_RPC_PORT" \
      RAFT_JOIN="$RAFT_JOIN_LIST" \
      RAFT_BOOTSTRAP_EXPECT=3 \
      DEFAULT_VECTORIZER_MODULE="text2vec-bigram" \
      ENABLE_MODULES="text2vec-bigram,backup-filesystem" \
      ../weaviate-server \
        --scheme=http \
        --host="127.0.0.1" \
        --port="$HTTP_PORT" \
        --raft-internal-rpc-port="$RAFT_INTERNAL_RPC_PORT" \
        > "../server_$i.log" 2>&1 &
  )
  
  echo "Waiting for node $i to be ready..."
  #while ! nc -z 127.0.0.1 $GOSSIP_PORT; do
  local COUNT=0
  while ! curl http://localhost:$HTTP_PORT/v1/.well-known/live --fail --silent; do
    COUNT=$((COUNT + 1))
    echo -n " $COUNT "
    if [ "$COUNT" -gt "$MAX_RETRIES" ]; then
      echo " - Exceeded maximum retries for node $i, quitting launch script."
      break
      #exit 1
    fi
  done
  echo "Node $i is up!"
}

# Start the first node and wait for it to be ready
start_weaviate 0

echo "Waiting for weaviate-0 to start before launching other nodes..."
while ! nc -z 127.0.0.1 $BASE_GOSSIP_PORT; do
  sleep 1
done
echo "weaviate-0 is ready, starting the rest of the cluster..."

# Start the remaining nodes
for i in $(seq 1 $((NUM_SERVERS - 1))); do
  start_weaviate "$i"
done

echo "Launched $NUM_SERVERS Weaviate servers."
