package main

import (
	"encoding/csv"

	"io"

	"bufio"
	"context"
	"encoding/json"
	"io/ioutil"
	"strconv"
	"time"

	"flag"
	"log"

	"github.com/donomii/goof"
	//"gitlab.com/donomii/weaviate-client"
	"os"
	"strings"

	"github.com/weaviate/weaviate-go-client/weaviate"
	"github.com/weaviate/weaviate-go-client/weaviate/auth"
	"github.com/weaviate/weaviate/entities/models"

	//"time"
	"fmt"
)

// {"category": "HISTORY", "air_date": "2004-12-31", "question": "'For the last 8 years of his life, Galileo was under house arrest for espousing this man's theory'", "value": "$200", "answer": "Copernicus", "round": "Jeopardy!", "show_number": "4680"}
type Jeopardy_strs struct {
	ShowNumber string `json:"show_number"`
	AirDate    string `json:"air_date"`
	Round      string `json:"round"`
	Category   string `json:"category"`
	Value      string `json:"value"`
	Question   string `json:"question"`
	Answer     string `json:"answer"`
}

type Jeopardy struct {
	ShowNumber int       `json:"show_number"`
	AirDate    time.Time `json:"air_date"`
	Round      string    `json:"round"`
	Category   string    `json:"category"`
	Value      int       `json:"value"`
	Question   string    `json:"question"`
	Answer     string    `json:"answer"`
}

func setupClient() *weaviate.Client {
	// Best practice: store your credentials in environment variables
	wcdURL := os.Getenv("WCD_DEMO_URL")
	wcdURL = "localhost:8080"
	wcdAPIKey := os.Getenv("WCD_DEMO_RO_KEY")
	//openaiAPIKey := os.Getenv("OPENAI_APIKEY")
	wcdScheme := os.Getenv("WCD_DEMO_SCHEME")
	wcdScheme = "http"
	if wcdScheme == "" {
		wcdScheme = "https"
	}

	var cfg weaviate.Config

	if strings.Contains(wcdURL, "localhost") {
		cfg = weaviate.Config{
			Host:   wcdURL,
			Scheme: wcdScheme,
		}
	} else {

		cfg = weaviate.Config{
			Host:   wcdURL,
			Scheme: wcdScheme,
			AuthConfig: auth.ApiKey{
				Value: wcdAPIKey,
			},
			//Headers: map[string]string{
			//	"X-OpenAI-Api-Key": openaiAPIKey,
			//},
		}
	}

	client, err := weaviate.NewClient(cfg)
	if err != nil {
		panic(fmt.Sprintf("Failed to create Weaviate client: %v", err))
	}

	return client
}

func main() {
	var jsonFile *bufio.Reader
	var inFile string
	var compression string

	flag.StringVar(&compression, "compression", "", "Input is compressed with bz2 or gz")
	flag.Parse()

	args := flag.Args()

	loadWineData("winemag_tiny.csv")

	cwd, _ := os.Getwd()
	fmt.Printf("current directory: %s\n", cwd)
	args = []string{"200k_questions.json.gz"}  //FIXME remove this
	///log.Println(args)
	if len(args) > 0 {
		inFile = args[0]
		log.Println("Reading from", inFile)
		if inFile == "-" {
			inFile = ""
		}
		jsonFile = goof.OpenBufferedInput(inFile, compression)
		//defer jsonFile.Close()
	} else {
		log.Println("Reading from stdin")
		jsonFile = goof.OpenBufferedInput("", compression)
	}
	/*
	   categories = client.collections.create(
	              name=self._category_collection,
	              vectorizer_config=self.vectorizer_config,
	              vector_index_config=self.vectorindex_config,
	              generative_config=self.generative_config,
	              reranker_config=self.reranker_config,
	              properties=[
	                  Property(
	                      name="title",
	                      data_type=DataType.TEXT,
	                      description="The category title",
	                  )
	              ],
	          )

	          questions = client.collections.create(
	              name=self._question_collection,
	              vectorizer_config=self.vectorizer_config,
	              vector_index_config=self.vectorindex_config,
	              generative_config=self.generative_config,
	              reranker_config=self.reranker_config,
	              inverted_index_config=Configure.inverted_index(
	                  index_property_length=True, index_timestamps=True, index_null_state=True
	              ),
	              properties=[
	                  Property(
	                      name="question",
	                      data_type=DataType.TEXT,
	                      description="Question asked to the contestant",
	                  ),
	                  Property(
	                      name="answer",
	                      data_type=DataType.TEXT,
	                      description="Answer provided by the contestant",
	                  ),
	                  Property(
	                      name="points", data_type=DataType.INT, description="Jeopardy points"
	                  ),
	                  Property(
	                      name="round",
	                      data_type=DataType.TEXT,
	                      description="Jeopardy round",
	                      tokenization=Tokenization.FIELD,
	                  ),
	                  Property(
	                      name="air_date",
	                      data_type=DataType.DATE,
	                      description="Date that the episode first aired on TV",
	                  ),
	              ],
	              references=[
	                  ReferenceProperty(
	                      name=self._xref_prop_name,
	                      target_collection="JeopardyCategory",
	                  ),
	              ],
	          )*/

	questions, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		log.Println("Error reading json", err)
		return
	}
	//log.Println(line)
	var js_strs []Jeopardy_strs
	err = json.Unmarshal([]byte(questions), &js_strs)
	if err != nil {
		log.Println("Error unmarshalling json", err)
		return
	}

	js := make([]Jeopardy, 0, len(js_strs))

	for _, j := range js_strs {
		air_date, err := time.Parse("2006-01-02", j.AirDate)
		if err != nil {
			log.Println("Error parsing date", err)
			return
		}
		show_number, err := strconv.Atoi(j.ShowNumber)
		points, err := strconv.Atoi(strings.Trim(j.Value, "$"))
		js = append(js, Jeopardy{
			ShowNumber: show_number,
			AirDate:    air_date,
			Round:      j.Round,
			Category:   j.Category,
			Value:      points,
			Question:   j.Question,
			Answer:     j.Answer,
		})
	}

	//Make a list of all the categories
	categories := make(map[string]bool)
	for _, question := range js {
		categories[question.Category] = true
	}

	category_list := make([]string, 0, len(categories))
	for k := range categories {
		category_list = append(category_list, k)
	}

	client := setupClient()
	err = client.Schema().ClassCreator().WithClass(&models.Class{
		Class: "JeopardyCategory",
		ModuleConfig: map[string]interface{}{
			"text2vec-gpt4all": map[string]interface{}{
				"vectorizeClassName": false,
				"poolingStrategy":    "cls",
			},
		},
		Properties: []*models.Property{
			{
				Name:     "title",
				DataType: []string{"text"},
			},
		},
	}).Do(context.Background())
	if err != nil {
		panic(fmt.Sprintf("Failed to create class: %v", err))
	}

	err = client.Schema().ClassCreator().WithClass(&models.Class{
		Class: "JeopardyQuestion",
		ModuleConfig: map[string]interface{}{
			"text2vec-gpt4all": map[string]interface{}{
				"vectorizeClassName": false,
				"poolingStrategy":    "cls",
			},
		},
		Properties: []*models.Property{
			{
				Name:     "question",
				DataType: []string{"text"},
			},
			{
				Name:     "answer",
				DataType: []string{"text"},
			},
			{
				Name:     "points",
				DataType: []string{"int"},
			},
			{
				Name:     "round",
				DataType: []string{"text"},
			},
			{
				Name:     "air_date",
				DataType: []string{"date"},
			},
			{
				Name:        "hasCategory",
				DataType:    []string{"JeopardyCategory"},
				Description: "The category of the question",
			},
		},
	}).Do(context.Background())
	if err != nil {
		panic(fmt.Sprintf("Failed to create class: %v", err))
	}

	for _, category := range category_list {
		_, err := client.Data().Creator().WithClassName("JeopardyCategory").WithProperties(map[string]interface{}{"title": category}).Do(context.Background())
		if err != nil {
			log.Println("Error inserting object", err)
		}
		fmt.Printf("Inserting category: %v\n", category)
	}

	for _, question := range js {
		/*
			obj := weaviate.ObjectInsert{
				Class: "JeopardyQuestion",
				Properties: map[string]interface{}{
					"question": question.Question,
					"answer":   question.Answer,
					"points":   question.Value,
					"round":    question.Round,
					"air_date": question.AirDate,
				},
			}

			objs := weaviate.ObjectsInsert{[]weaviate.ObjectInsert{obj}}
			_, err = weaviate.InsertObjects(objs)
			if err != nil {
				log.Println("Error inserting object", err)

			}
		*/

		_, err := client.Data().Creator().WithClassName("JeopardyQuestion").WithProperties(map[string]interface{}{
			"question": question.Question,
			"answer":   question.Answer,
			"points":   question.Value,
			"round":    question.Round,
			"air_date": question.AirDate}).Do(context.Background())

		if err != nil {
			log.Println("Error inserting object", err)
		}
		fmt.Printf("Question: %v, Answer: %v\n", question.Question, question.Answer)
	}
	//log.Println(j)
}

type Wine struct {
	Country             string
	Description         string
	Designation         string
	Points              int
	Price               float64
	Province            string
	Region1             string
	Region2             string
	TasterName          string
	TasterTwitterHandle string
	Title               string
	Variety             string
	Winery              string
	DescLen             int
}

func loadWineData(filename string) {
	// Open the CSV file
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// Create a new CSV reader
	reader := csv.NewReader(file)

	// Read and discard the header
	_, err = reader.Read()
	if err != nil {
		log.Fatal(err)
	}

	// Slice to store the wines
	var wines []Wine

	// Read the rest of the records
	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		// Parse the data and create a Wine struct
		points, _ := strconv.Atoi(record[3])
		price, _ := strconv.ParseFloat(record[4], 64)
		descLen, _ := strconv.Atoi(record[13])

		wine := Wine{
			Country:             record[0],
			Description:         record[1],
			Designation:         record[2],
			Points:              points,
			Price:               price,
			Province:            record[5],
			Region1:             record[6],
			Region2:             record[7],
			TasterName:          record[8],
			TasterTwitterHandle: record[9],
			Title:               record[10],
			Variety:             record[11],
			Winery:              record[12],
			DescLen:             descLen,
		}

		wines = append(wines, wine)
	}

	fmt.Printf("Loading %d wines\n", len(wines))
	client := setupClient()
	// Print the parsed data
	for _, wine := range wines {
		client.Data().Creator().WithClassName("WineReviewNV").WithProperties(map[string]interface{}{
			"country":               wine.Country,
			"description":           wine.Description,
			"designation":           wine.Designation,
			"points":                wine.Points,
			"price":                 wine.Price,
			"province":              wine.Province,
			"region1":               wine.Region1,
			"region2":               wine.Region2,
			"taster_name":           wine.TasterName,
			"taster_twitter_handle": wine.TasterTwitterHandle,
			"title":                 wine.Title,
			"variety":               wine.Variety,
			"winery":                wine.Winery,
			"desc_len":              wine.DescLen,
		}).Do(context.Background())

		if err != nil {
			log.Println("Error inserting object", err)
		}
		fmt.Printf(".")
	}

}
