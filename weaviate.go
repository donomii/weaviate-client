package weaviate

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"

	"time"

	"context"
	"crypto/tls"
	"strings"


	pb "gitlab.com/donomii/weaviate-client/grpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	structpb "google.golang.org/protobuf/types/known/structpb"

	"github.com/google/uuid"
	"github.com/weaviate/weaviate/entities/models"

	"github.com/charmbracelet/lipgloss"
	"google.golang.org/grpc/health/grpc_health_v1"
)

// Enhanced color styles for visual appeal
var (
	// Primary styles
	headerStyle  = lipgloss.NewStyle().Bold(true).Foreground(lipgloss.Color("#61AFEF")).Border(lipgloss.NormalBorder(), false, false, true, false).BorderForeground(lipgloss.Color("#61AFEF")).PaddingBottom(1)
	successStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("#98C379")).Bold(true)
	errorStyle   = lipgloss.NewStyle().Foreground(lipgloss.Color("#E06C75")).Bold(true)
	infoStyle    = lipgloss.NewStyle().Foreground(lipgloss.Color("#E5C07B"))
	
	// Result styles
	idStyle      = lipgloss.NewStyle().Foreground(lipgloss.Color("#56B6C2")).Bold(true)
	scoreStyle   = lipgloss.NewStyle().Foreground(lipgloss.Color("#C678DD"))
	propKeyStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("#61AFEF"))
	propValStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("#ABB2BF"))
	
	// Table styles
	tableHeaderStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("#61AFEF")).Bold(true).Underline(true)
	dividerStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("#565C64"))
)

// Maximum content length for displayed values
const MaxContentLength = 60





// Pretty print JSON with improved formatting
func prettyPrintJSON(data interface{}) {
	jsonData, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		logError("Failed to format JSON output")
		fmt.Println(data)
	} else {
		// Simple JSON highlighting
		jsonStr := string(jsonData)

		// Highlight keys with regex
		re := regexp.MustCompile(`"([^"]+)":`)
		jsonStr = re.ReplaceAllString(jsonStr, propKeyStyle.Render(`"$1":`) + " ")

		fmt.Println(jsonStr)
	}
}

// logError handles both formatted messages and direct errors with improved styling
func logError(format string, args ...interface{}) {
	if len(args) == 1 {
		if err, ok := args[0].(error); ok {
			fmt.Println(errorStyle.Render(fmt.Sprintf("ERROR: %v", err)))
			return
		}
	}
	fmt.Println(errorStyle.Render(fmt.Sprintf("ERROR: "+format, args...)))
}

// logSuccess prints success messages with improved styling
func logSuccess(format string, args ...interface{}) {
	fmt.Println(successStyle.Render(fmt.Sprintf("SUCCESS: "+format, args...)))
}

// logInfo prints info messages with improved styling
func logInfo(format string, args ...interface{}) {
	fmt.Println(infoStyle.Render(fmt.Sprintf("INFO: "+format, args...)))
}





// WeaviateUrl is the URL of the weaviate server.  Be sure to include the /v1/ and trailing slash
var WeaviateUrl = "http://localhost:8080/v1/"

/*
{
	"hostname": "http://[::]:8080",
	"modules": {
		"qna-transformers": {
			"model": {
				"_name_or_path": "./models/model",
				"add_cross_attention": false,
				"architectures": [
					"BertForQuestionAnswering"
				],
*/

// ObjectsInsert a list of objects to insert
type ObjectsInsert struct {
	Objects []ObjectInsert
}

// ObjectInsert is a single object to insert
type ObjectInsert struct {
	Id         string                 `json:"id"`
	Class      string                 "json:\"class\""
	Properties map[string]interface{} "json:\"properties\""
	Vector     []float32              "json:\"vector\""
}

type Module struct {
	Model struct {
		NameOrPath    string   `json:"_name_or_path"`
		Architectures []string `json:"architectures"`
	} `json:"model"`
}

type InfoResponse struct {
	Hostname string `json:"hostname"`
	Modules  map[string]Module
}

// The response to a Q&A query
type AskResponse struct {
	Data struct {
		Get struct {
			Page []struct {
				Content    string `json:"content"`
				Additional struct {
					Answer struct {
						Certainty     float64 `json:"certainty"`
						EndPosition   int     `json:"endPosition"`
						HasAnswer     bool    `json:"hasAnswer"`
						Property      string  `json:"property"`
						Result        string  `json:"result"`
						StartPosition int     `json:"startPosition"`
					} `json:"answer"`
				} `json:"_additional"`
			} `json:"Page"`
		} `json:"Get"`
	} `json:"data"`
	Errors []struct {
		Message   string
		Locations []struct {
			Line   int "json:\"line\""
			Column int "json:\"column\""
		} "json:\"locations\""
	} "json:\"errors\""
}

type Resp struct {
	Data struct {
		Get struct {
			RespItems map[string][]Result
		}
	}
	Errors []struct {
		Message   string
		Locations []struct {
			Line   int "json:\"line\""
			Column int "json:\"column\""
		} "json:\"locations\""
	} "json:\"errors\""
}

type Result struct {
	Content string "json:\"content\""
	Url     string "json:\"url\""
	Title   string "json:\"title\""
}

type resultFlexi struct {
	Data   map[string]map[string][]map[string]interface{} `json:"data"`
	Errors []struct {
		Message   string
		Locations []struct {
			Line   int "json:\"line\""
			Column int "json:\"column\""
		} "json:\"locations\""
	} "json:\"errors\""
}

// Normalise a vector
func NormaliseVector(vector []float32) []float32 {
	var sum float32
	for _, v := range vector {
		sum += v
	}

	for i, v := range vector {
		vector[i] = v / sum
	}
	return vector
}

// Calculate a bigram vector by doing mod26 on the letters
func Mod26Vector(input string) ([]float32, error) {
	input = strings.ToLower(input)
	vector := make([]float32, 26*26)
	for i := 0; i < len(input)-1; i++ {
		first := int(input[i]) % 26
		second := int(input[i+1]) % 26
		index := first*26 + second
		vector[index] = vector[index] + 1
	}

	return NormaliseVector(vector), nil
}

// InsertObjectsToNode inserts a list of objects into the database at a specific node.
func InsertObjectsToNode(objects ObjectsInsert, nodeURL string) ([]byte, error) {
	url := nodeURL + "/v1/batch/objects"
	json, err := json.Marshal(objects)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	// Upload the objects using a HTTP POST request
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(json))
	if resp != nil && resp.Body != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		log.Println(err)
		return nil, err
	}

	// Read the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	if !strings.Contains(string(body), "creationTimeUnix") {
		log.Println("Error inserting objects")
		log.Println(string(body))
		return nil, fmt.Errorf("Error inserting objects %v", string(body))
	}

	return body, nil
}

// InsertObjects inserts a list of objects into the database.  Place your objects in the ObjectsInsert struct.  Returns the server response and an error
func InsertObjects(objects ObjectsInsert) ([]byte, error) {

	url := WeaviateUrl + "batch/objects"
	json, err := json.Marshal(objects)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	var resp *http.Response

	// Upload the objects using a HTTP POST request
	resp, err = http.Post(url, "application/json", bytes.NewBuffer(json))
	if resp != nil && resp.Body != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		log.Println(err)
		return nil, err
	}

	// Read the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	if !strings.Contains(string(body), "creationTimeUnix") {
		log.Println("Error inserting objects")
		log.Println(string(body))
		return nil, fmt.Errorf("Error inserting objects %v", string(body))
	}
	//log.Println(goof.ShortenString(80, string(body)))
	return body, nil
}

// Perform a semantic query, using the  nearText option
func Semantic(q []string) []string {
	quotedArgs := make([]string, len(q))
	for i, arg := range q {
		quotedArgs[i] = fmt.Sprintf("\\\"%v\\\"", arg)
	}
	// Join the quoted args with spaces
	quotedArgsString := strings.Join(quotedArgs, ",")

	//logError("args: %v\n\n", quotedArgsString)

	query := fmt.Sprintf(`
	{"query":"{Get {Page (nearText: {concepts: [%v]}){content}}}","variables":{}}`, quotedArgsString)

	logError("query: %v\n\n", query)

	url := WeaviateUrl + "graphql"
	resp, err := http.Post(url, "application/json", bytes.NewBuffer([]byte(query)))
	if err != nil {
		log.Println(err)
		return nil
	}
	defer resp.Body.Close()
	// Print the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return nil
	}
	// log.Println(string(body))

	// Unmarshal the response
	var result Resp
	err = json.Unmarshal(body, &result)

	if err != nil {
		log.Println(err)
		return nil
	}
	resultsClasses := result.Data.Get.RespItems
	errs := result.Errors
	if len(errs) > 0 {
		logError("Semantic Errors: %v\n", errs)
		return nil
	}
	// Filter out any result containing a pdf, return the first 3

	out := []string{}
	for _, results := range resultsClasses {
		//logError("resultsClass: %v\n", resultsClass)
		for i, result := range results {
			if !strings.Contains(result.Content, "PDF") && i < 3 {
				out = append(out, result.Content)
			}
		}
	}

	return out
}

/*
// PErform a generative query
func Generative ( class string,q string, limit int) string {

	query := fmt.Sprintf(`{"query":"{Get{%v(generate(singleResult:{prompt:\"%v\"}){error singleResult}){}}}`, class, q) //fixme remove hardcoded title

	logError("Using GraphQL query: %v\n\n", query)

	url := WeaviateUrl + "graphql"
	resp, err := http.Post(url, "application/json", bytes.NewBuffer([]byte(query)))
	if err != nil {
		log.Println(err)
		return ""
	}
	defer resp.Body.Close()
	// Print the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return ""
	}
	log.Println(string(body))

	return string(body)
}
*/

// Preform a hybrid query, return the results in the flexible result format
func HybridRaw(q []string, class string, limit int) resultFlexi {

	// Join the quoted args with spaces
	quotedArgsString := strings.Join(q, " ")

	//logError("args: %v\n\n", quotedArgsString)
	properties := GetProperties_str(class)
	query := fmt.Sprintf(`{"query":"{Get {%v (limit: %v, hybrid: {alpha:0.5, query: \"%v\" }){%v _additional {score id}}}}","variables":{}}`, class, limit, quotedArgsString, properties) //fixme remove hardcoded title

	logError("GraphQL query: %v\n\n", query)

	url := WeaviateUrl + "graphql"
	resp, err := http.Post(url, "application/json", bytes.NewBuffer([]byte(query)))
	if err != nil {
		log.Println(err)
		return resultFlexi{}
	}
	defer resp.Body.Close()
	// Print the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return resultFlexi{}
	}
	//log.Println(string(body))

	// Unmarshal the response
	var result resultFlexi
	err = json.Unmarshal(body, &result)

	if err != nil {
		log.Println(err)
		return resultFlexi{}
	}

	//log.Println(result.Data)

	return result
}

// Perform a hybrid search, return the results as a list of strings
func HybridSearch(q []string, class string, limit int) []string {

	result := HybridRaw(q, class, limit)

	resultsClasses := result.Data["Get"]

	errs := result.Errors
	if len(errs) > 0 {
		logError("Hybrid Errors: %v\n", errs)
		return nil
	}
	// Filter out any result containing a pdf, return the first 3

	out := []string{}
	for _, results := range resultsClasses {
		//logError("resultsClass: %v\n", resultsClass)
		for _, result := range results {
			//iterate over the properties, add them to the return value
			for property, value := range result {
				if property == "_additional" {
					for additionalProperty, additionalValue := range value.(map[string]interface{}) {
						if additionalProperty == "score" {
							out = append(out, fmt.Sprintf("Score: %v", additionalValue))

						}
						if additionalProperty == "id" {
							out = append(out, fmt.Sprintf("ID: %v", additionalValue))

						}
					}
				} else {
					out = append(out, fmt.Sprintf("%v: %v", property, value))
				}

			}
		}
	}

	return out
}

// Perform a hybrid search, return the results as a list of strings
func HybridPropResults(q []string, class string, limit int) []map[string]string {

	result := HybridRaw(q, class, limit)

	errs := result.Errors
	if len(errs) > 0 {
		logError("HybridPropResults Errors: %v\n", errs)
		return nil
	}

	out := UnpackResults(result)

	return out
}

// Preform a bm25f query, return the results in the flexible result format
func BM25fRaw(q string, class string, limit int) resultFlexi {

	// Join the quoted args with spaces
	quotedArgsString := q

	//logError("args: %v\n\n", quotedArgsString)
	properties := GetProperties_str(class)
	query := fmt.Sprintf(`{"query":"{Get {%v (limit: %v, bm25: { query: \"%v\" }){%v _additional {score id}}}}","variables":{}}`, class, limit, quotedArgsString, properties) //fixme remove hardcoded title

	logError("GraphQL query: %v\n\n", query)

	url := WeaviateUrl + "graphql"
	resp, err := http.Post(url, "application/json", bytes.NewBuffer([]byte(query)))
	if err != nil {
		log.Println(err)
		return resultFlexi{}
	}
	defer resp.Body.Close()
	// Print the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return resultFlexi{}
	}
	//log.Println(string(body))

	// Unmarshal the response
	var result resultFlexi
	err = json.Unmarshal(body, &result)

	if err != nil {
		log.Println(err)
		return resultFlexi{}
	}

	//log.Println(result.Data)

	return result
}

// Perform a bm25f search, return the results as a list of strings
func KeywordPropResults(q []string, class string, limit int) []map[string]string {

	search := strings.Join(q, " ")
	result := BM25fRaw(search, class, limit)

	errs := result.Errors
	if len(errs) > 0 {
		logError("KeywordPropResults Errors: %v\n", errs)
		return nil
	}

	out := UnpackResults(result)

	return out
}

// Preform a semantic (near text) query, return the results in the flexible result format
func VectorPropResults(q []string, class string, limit int) []map[string]string {

	search := strings.Join(q, " ")
	results := VectorSearch(class, search, limit)

	return results
}

func BM25fPropResultsWithFilter(class, path string, limit int, filters ...Filter) []map[string]string {
	// Construct the filter string
	filterStr := constructFilterString(filters)

	// Call the raw function with the filter
	result := BM25fRawWithFilter(class, path, limit, filterStr)

	errs := result.Errors
	if len(errs) > 0 {
		logError("BM25fPropResultsWithFilter Errors: %v\n", errs)
		return nil
	}

	out := UnpackResults(result)

	return out
}

// Filter struct to hold filter information
type Filter struct {
	Path     string
	Operator string
	Value    interface{}
}

// Helper function to construct the filter string
func constructFilterString(filters []Filter) string {
	if len(filters) == 0 {
		return ""
	}

	var filterStrs []string
	for _, f := range filters {
		valueType := getValueType(f.Value)
		quotedValue := quoteValue(f.Value)
		filterStr := fmt.Sprintf(`{ path: [\"%s\"], operator: %s, value%s: %v }`,
			f.Path, f.Operator, valueType, quotedValue)
		filterStrs = append(filterStrs, filterStr)
	}

	if len(filterStrs) == 1 {
		return "where: " + filterStrs[0]
	} else {
		return "where: { operator: And, operands: [" + strings.Join(filterStrs, ", ") + "] }"
	}
}

func quoteValue(value interface{}) string {
	switch value.(type) {
	case string:
		return fmt.Sprintf(`\"%v\"`, value)
	default:
		return ""
	}
}

// Modify BM25fRawWithFilter to accept a filter string
func BM25fRawWithFilter(class, path string, limit int, filterStr string) resultFlexi {

	properties := GetProperties_str(class)

	query := fmt.Sprintf(`{"query": "{ Get { %s ( limit: %v %s ) { %v _additional {score id} }}}","variables": {}}`, class, limit, filterStr, properties)

	logError("GraphQL: \n%v\n\n", query)

	url := WeaviateUrl + "graphql"
	resp, err := http.Post(url, "application/json", bytes.NewBuffer([]byte(query)))
	if err != nil {
		log.Println(err)
		return resultFlexi{}
	}
	defer resp.Body.Close()

	// Print the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return resultFlexi{}
	}

	// Unmarshal the response
	var result resultFlexi
	err = json.Unmarshal(body, &result)
	if err != nil {
		log.Println(err)
		return resultFlexi{}
	}

	return result
}

// getValueType function remains the same as in the previous example

// Helper function to determine the value type for the GraphQL query
func getValueType(value interface{}) string {
	switch value.(type) {
	case int, int32, int64:
		return "Int"
	case float32, float64:
		return "Number"
	case string:
		return "Text"
	case bool:
		return "Boolean"
	default:
		return "String" // Default to String for unknown types
	}
}

func BM25fPropResults(q string, class string, limit int) []map[string]string {

	result := BM25fRaw(q, class, limit)

	errs := result.Errors
	if len(errs) > 0 {
		logError("BM25fPropResults Errors: %v\n", errs)
		return nil
	}

	out := UnpackResults(result)

	return out
}

func BM25fPropResultsMap(q string, class string, limit int) []map[string]interface{} {

	result := BM25fRaw(q, class, limit)

	errs := result.Errors
	if len(errs) > 0 {
		logError("BM25fPropResultsMap Errors: %v\n", errs)
		return nil
	}

	out := UnpackResultsInterface(result)

	return out
}

// Get a list of cluster nodes
func Nodes() string {
	// Get nodes list from GET /v1/nodes
	url := WeaviateUrl + "nodes"
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
		return ""
	}
	defer resp.Body.Close()
	// Print the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return ""
	}
	log.Println(string(body))

	return string(body)

}

func SearchByProperty(class, property, text string) []map[string]string {
	properties := GetProperties_str(class)
	query := fmt.Sprintf(`{"query":"{Get {%v (limit: %v, bm25: { query: \"%v\", properties: [\"%v\"]}){%v _additional {score id}}}}","variables":{}}`, class, 9999, text, property, properties)

	logError("GraphQL query: %v\n\n", query)

	url := WeaviateUrl + "graphql"
	resp, err := http.Post(url, "application/json", bytes.NewReader([]byte(query)))

	if err != nil {
		log.Println(err)
		return []map[string]string{}
	}
	defer resp.Body.Close()
	// Print the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return []map[string]string{}
	}
	log.Println(string(body))

	// Unmarshal the response
	var result resultFlexi
	err = json.Unmarshal(body, &result)

	if err != nil {
		log.Println(err)
		return []map[string]string{}
	}

	//log.Println(result.Data)

	out := UnpackResults(result)

	return out
}

func UnpackResults(result resultFlexi) []map[string]string {

	//	fmt.Printf("Unpacking results: %+v\n", result)

	resultsClasses := result.Data["Get"]

	errs := result.Errors

	if len(errs) > 0 {
		logError("UnpackResults Errors: %v\n", errs)
		return nil
	}

	out := []map[string]string{}
	for _, results := range resultsClasses {
		//logError("resultsClass: %v\n", resultsClass)
		logError("Found %v results\n", len(results))
		for _, result := range results {
			//iterate over the fields, add them to the return value
			propmap := make(map[string]string)
			for property, value := range result {
				//Value is []map[string]string
				//loop over the array, find every

				if property == "_additional" {
					propmap["DocId"] = fmt.Sprintf("%v", value.(map[string]interface{})["id"])
					propmap["Score"] = fmt.Sprintf("%v", value.(map[string]interface{})["score"])
				} else {
					propmap[property] = fmt.Sprintf("%v", value)
				}
			}
			out = append(out, propmap)
		}

	}
	return out
}

func UnpackResultsInterface(result resultFlexi) []map[string]interface{} {

	//	fmt.Printf("Unpacking results: %+v\n", result)

	resultsClasses := result.Data["Get"]

	errs := result.Errors

	if len(errs) > 0 {
		logError("UnpackResultsInterface Errors: %v\n", errs)
		return nil
	}

	out := []map[string]interface{}{}
	for _, results := range resultsClasses {
		//logError("resultsClass: %v\n", resultsClass)
		logError("Found %v results\n", len(results))
		for _, result := range results {
			//iterate over the fields, add them to the return value
			propmap := make(map[string]interface{})
			for property, value := range result {
				//Value is []map[string]string
				//loop over the array, find every

				if property == "_additional" {
					propmap["DocId"] = value.(map[string]interface{})["id"]
					propmap["Score"] = value.(map[string]interface{})["score"]
				} else {
					propmap[property] = value
				}
			}
			out = append(out, propmap)
		}

	}
	return out
}

// Get the properties of a class, as a space separated string
func GetProperties(class string) []string {
	url := WeaviateUrl + "schema/" + class
	//log.Println("Fetching properties for class: " + class + " from url: " + url)
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
		return []string{}
	}
	defer resp.Body.Close()
	// Print the response

	//Unmarshal the response
	var result TypeSafeClass
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		log.Println(err)
		return []string{}
	}

	completeProperties := result.Properties
	properties := []string{}
	for _, property := range completeProperties {
		if property.DataType[0] == "string" || property.DataType[0] == "text" {
			properties = append(properties, property.Name)
		}
	}
	return properties
}

func GetProperties_str(class string) string {
	return strings.Join(GetProperties(class), " ")
}

func Info() string {
	url := WeaviateUrl + "meta"
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
		return "Failed to get info" + err.Error()
	}
	defer resp.Body.Close()
	// Print the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return "Failed to get info" + err.Error()
	}
	// log.Println(string(body))

	// Unmarshal the response
	var result InfoResponse
	err = json.Unmarshal(body, &result)

	if err != nil {
		log.Println(err)
		return ""
	}

	// logError("result: %+v\n\n", result)

	for k, mod := range result.Modules {
		logError("Module: %v: %v\n", k, mod.Model.Architectures)
	}
	return ""
}

// Ask a question using the Q&A module
func AskQuestion(class, question string, limit int) []string {
	query := fmt.Sprintf(`{"query":"{Get {  %v( ask: {  question: \"%v\",    rerank: false }, limit: %v  ) { %v }}}","variables":{}}`, class, question, limit, GetProperties_str(class))

	logError("query: %v\n\n", query)

	url := WeaviateUrl + "graphql"
	resp, err := http.Post(url, "application/json", bytes.NewBuffer([]byte(query)))
	if err != nil {
		log.Println(err)
		return nil
	}
	defer resp.Body.Close()
	// Print the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return nil
	}
	// log.Println(string(body))

	// Unmarshal the response
	var result AskResponse
	err = json.Unmarshal(body, &result)

	if err != nil {
		log.Println(err)
		return nil
	}

	results := result.Data.Get.Page
	errs := result.Errors
	if len(errs) > 0 {
		logError("AskQuestion Errors: %v\n", errs)
		return nil
	}
	// Filter out any result containing a pdf, return the first 3

	out := []string{}

	for i, result := range results {
		res := result.Additional.Answer.Result
		cont := result.Content

		if !strings.Contains(cont, "PDF") && i < 3 {
			out = append(out, res)
		}
	}

	return out
}

// TEst if the server is running
func Probe() {
	conn, _ := CreateGrpcConnectionClient(":50051")

	client := grpc_health_v1.NewHealthClient(conn)
	check, err := client.Check(context.TODO(), &grpc_health_v1.HealthCheckRequest{})
	logError("Probe response: %v, %v", check, err)
}

func Httpcall(method, url string, body []byte) (*http.Response, error) {
	// Make io reader from body
	bodyReader := bytes.NewReader(body)
	req, err := http.NewRequest(method, url, bodyReader)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	client := http.DefaultClient
	return client.Do(req)
}

// Does a REST call with the supplied method, url and body, return the status and body of the response
func DoRESTCall(method, url string, body interface{}) (string, []byte) {
	// Serialize body if provided
	var jsonData []byte
	var err error
	if body != nil {
		jsonData, err = json.Marshal(body)
		if err != nil {
			logError("Failed to serialize request body: %v", err)
			return "", nil
		}
	}

	// Prepare request
	req, err := http.NewRequest(method, url, bytes.NewBuffer(jsonData))
	if err != nil {
		logError("Failed to create request: %v", err)
		return "", nil
	}
	req.Header.Set("Content-Type", "application/json")

	// Execute request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		logError("HTTP request failed: %v", err)
		return "", nil
	}
	defer resp.Body.Close()

	// Read response body
	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logError("Failed to read response: %v", err)
		return "", nil
	}

	// Print formatted JSON response for debugging
	log.Printf("Response %v from %s %s\n", resp.StatusCode, method, url)
	//prettyPrintJSON(responseBody)

	return resp.Status, responseBody
}

// List all the classes on the server
func GetClasses() []string {
	_, body := DoRESTCall("GET", WeaviateUrl+"schema", nil)
	var schm Schema
	err := json.Unmarshal(body, &schm)
	if err != nil {
		log.Println(err)
		return nil
	}
	// Make string list of classes
	classes1 := []string{}
	for _, class := range schm.Classes {
		classes1 = append(classes1, class.Class)
	}
	return classes1
}

// Support command completion for classes
func CompleteClasses() func(string) []string {
	return func(line string) []string {
		classes := GetClasses()
		//log.Print(classes)
		return classes
	}
}

func GetPropertiesForClass(matchclass string) []string {
	_, body := DoRESTCall("GET", WeaviateUrl+"schema/"+matchclass, nil)
	var schm Schema
	err := json.Unmarshal(body, &schm)
	if err != nil {
		log.Println(err)
		return nil
	}
	// Make string list of classes
	properties := []string{}
	for _, class := range schm.Classes {
		if class != nil && class.Class == matchclass {

			for _, prop := range class.Properties {
				properties = append(properties, prop.DataType[0])
			}
		}
	}
	return properties
}

func CompleteProperties(matchclass string) func(string) []string {
	return func(line string) []string {
		properties := GetPropertiesForClass(matchclass)
		//log.Print(properties)
		return properties
	}
}

type Tenant struct {

	// activity status of the tenant's shard. Optional for creating tenant (implicit `HOT`) and required for updating tenant. Allowed values are `HOT` - tenant is fully active, `WARM` - tenant is active, some restrictions are imposed (TBD; not supported yet), `COLD` - tenant is inactive; no actions can be performed on tenant, tenant's files are stored locally, `FROZEN` - as COLD, but files are stored on cloud storage (not supported yet)
	// Enum: [HOT WARM COLD FROZEN]
	ActivityStatus string `json:"activityStatus,omitempty"`

	// name of the tenant
	Name string `json:"name,omitempty"`
}

func GetTenants(class string) []string {
	_, responseData := DoRESTCall("GET", WeaviateUrl+"schema/"+class+"/tenants", nil)

	var tenants []Tenant
	json.Unmarshal(responseData, &tenants)
	out := []string{}
	for _, tenant := range tenants {
		out = append(out, tenant.Name)
	}
	return out
}

// [{"name":"KmQGi9RKT1iQ","status":"READY"}]
// List all the shards for a class
func GetShards(class string) [][]string {
	_, body := DoRESTCall("GET", WeaviateUrl+"schema/"+class+"/shards", nil)
	// unmarshal json into an array of objects
	var shards []struct {
		Name   string "json:\"name\""
		Status string "json:\"status\""
	}
	err := json.Unmarshal(body, &shards)
	if err != nil {
		log.Println(err)
		return nil
	}
	// Make string list of classes
	shards1 := [][]string{}
	for _, shard := range shards {
		shards1 = append(shards1, []string{shard.Name, shard.Status})
	}
	return shards1
}

// Support command completion for shards
func CompleteShards() func(string) []string {
	return func(line string) []string {
		out := []string{}
		shards := GetShards(line)
		for _, shard := range shards {
			out = append(out, shard[0])
		}
		return out
	}
}

/*
{
	"data": {
		"Get": {
			"Page": [
				{
					"_additional": {
						"id": "002f5953-69c7-4797-814d-7bce0099aa06"
					},
					"title": "https://weaviate.io/developers/weaviate/current/client-libraries/javascript.html#v2120"
				},
*/

type getObjectsRequest struct {
	Query string "json:\"query\""
}

// Dump all the objects for a class.  In anything except a tiny database, this will probably not work
func GetObjects(class string, totalLimit int) []map[string]string {
	out := []map[string]string{}
	properties := GetProperties_str(class)
	after := ""
	count := 0
	for {
		if totalLimit > 0 && count > totalLimit {
			return out
		}
		fmt.Println("Retrieved", count, "objects")
		var reqBody getObjectsRequest
		if after == "" {
			reqBody = getObjectsRequest{Query: "{Get{" + class + " (limit: 999) {" + properties + " _additional {id score vector}}}}"}
		} else {
			reqBody = getObjectsRequest{Query: "{Get{" + class + " (limit: 999, after: \"" + after + "\") {" + properties + " _additional {id score vector}}}}"}
		}

		fmt.Println(reqBody)
		//}{Query: "{Get{" + class + " {" + properties + " _additional {id score}}}}"}

		// do rest call
		_, body := DoRESTCall("POST", WeaviateUrl+"graphql", reqBody)
		logError("body: %v\n", string(body))

		// Unmarshal the response
		var result resultFlexi
		err := json.Unmarshal(body, &result)

		if err != nil {
			log.Println(err)
			return []map[string]string{}
		}

		resultsClasses := result.Data["Get"]

		errs := result.Errors
		if len(errs) > 0 {
			logError("GetObjects Errors: %v\n", errs)
			return nil
		}

		if len(resultsClasses) == 0 {
			return out
		}
		lastCount := count
		for resultsClass, results := range resultsClasses {
			logError("resultsClass: %v\n", resultsClass)
			for _, result := range results {
				//iterate over the fields, add them to the return value
				propmap := make(map[string]string)
				for property, value := range result {
					//Value is []map[string]string
					//loop over the array, find every

					if property == "_additional" {
						propmap["DocId"] = fmt.Sprintf("%v", value.(map[string]interface{})["id"])
						propmap["Score"] = fmt.Sprintf("%v", value.(map[string]interface{})["score"])
						propmap["Vector"] = fmt.Sprintf("%v", value.(map[string]interface{})["vector"])
					} else {
						propmap[property] = fmt.Sprintf("%v", value)
					}
				}
				out = append(out, propmap)
				after = propmap["DocId"]
				count++
			}

		}

		if count == lastCount {
			return out
		}
	}

}

/*{
	"class": "Code",
	"creationTimeUnix": 1683325622153,
	"id": "6de1bb40-b3a4-4cd3-b9e4-efe4ace1a93e",
	"lastUpdateTimeUnix": 1683325622153,
	"properties": {
		"argument_types": "string, []byte, ",
		"end_pos": 3013,
		"filename": "/tmp/weaviate-client/goloader/main.go",
		"name": "ToWeaviate",
		"package": "main",
		"position": 3,
		"source": "// cts(objs)\n}\n",
		"start_pos": 3013
	},
	"vectorWeights": null
}
*/

type GetObjectResult struct {
	Class              string                 "json:\"class\""
	CreationTimeUnix   int64                  "json:\"creationTimeUnix\""
	Id                 string                 "json:\"id\""
	LastUpdateTimeUnix int64                  "json:\"lastUpdateTimeUnix\""
	Properties         map[string]interface{} "json:\"properties\""
	Vector             []float32              "json:\"vectorWeights\""
}

func GetObject(id string) GetObjectResult {
	// do rest call
	_, body := DoRESTCall("GET", WeaviateUrl+"objects/"+id, nil)
	logError("body: %v\n", string(body))

	// Unmarshal the response
	var result GetObjectResult
	err := json.Unmarshal(body, &result)

	if err != nil {
		log.Println(err)
		return GetObjectResult{}
	}

	return result
}

func UpdateObject(classname, id string, object GetObjectResult) {
	// do rest call
	_, body := DoRESTCall("PUT", WeaviateUrl+"objects/"+classname+"/"+id, object)
	//Read the body
	prettyPrintJSON(body)
}

func DeleteObjectFromNode(class, id, nodeURL string) error {
	url := fmt.Sprintf("%s/v1/objects/%s/%s", nodeURL, class, id)
	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusNoContent {
		body, _ := ioutil.ReadAll(resp.Body)
		return fmt.Errorf("unexpected status code: %d, body: %s", resp.StatusCode, string(body))
	}

	return nil
}

func DeleteObject(classname, id string) error {
	// do rest call
	status, body := DoRESTCall("DELETE", WeaviateUrl+"objects/"+classname+"/"+id, nil)
	//Read the body
	//logError("body: %v\n", string(body))
	if !strings.HasPrefix(string(status), "2") {
		log.Println("Error deleting object", string(body))
		return fmt.Errorf("Error deleting object %v", string(body))
	}
	return nil

}

func SetSchema(jsonschema []byte) {

	//Unmarshal the file into a Classes struct
	var schm Schema
	err := json.Unmarshal(jsonschema, &schm)
	if err != nil {
		log.Println("Error unmarshalling file:", err)
		return
	}

	logError("Found %v classes\n", len(schm.Classes))
	// Upload the classes, one by one
	for _, class := range schm.Classes {
		_, oldClass := DoRESTCall("GET", WeaviateUrl+"schema/"+class.Class, nil)
		logError("Deleting class %v\n", class.Class)
		resp, body := DoRESTCall("DELETE", WeaviateUrl+"schema/"+class.Class, nil)
		// if resp is not successful
		if !strings.HasPrefix(resp, "2") {
			log.Println("Error deleting class:", resp, string(body))
			continue
		}
		logError("Uploading class %v\n", class.Class)
		if !UploadClass(*class) {
			// unmarshall oldClass into oldClass_s
			var oldClass_s TypeSafeClass
			err := json.Unmarshal(oldClass, &oldClass_s)
			if err != nil {
				log.Println("Error unmarshalling old class:", err)
				continue
			}
			UploadClass(oldClass_s)
		}
	}

	log.Println("Done")
}

func UploadSchemaFromFile(filename string) {
	// Read file
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Println("Error reading file:", err)
		return
	}
	//Unmarshal the file into a Classes struct
	var schm Schema
	err = json.Unmarshal(file, &schm)
	if err != nil {
		log.Println("Error unmarshalling file:", err)
		return
	}

	logError("Found %v classes\n", len(schm.Classes))
	// Upload the classes, one by one
	for _, class := range schm.Classes {
		logError("Uploading class %v\n", class.Class)
		UploadClass(*class)
	}

	log.Println("Done")
}

func GetSchema() Schema {
	body := GetRawSchema()
	schm := Schema{}
	//log.Println(string(body))
	err := json.Unmarshal(body, &schm)
	if err != nil {
		log.Println(err)
		return schm
	}
	return schm
}

func GetRawSchema() []byte {
	_, body := DoRESTCall("GET", WeaviateUrl+"schema", nil)
	return body
}

func UploadClass(class TypeSafeClass) bool {

	// Make REST call
	resp, body := DoRESTCall("POST", WeaviateUrl+"schema", class)
	log.Println("Upload:", resp, string(body))
	if !strings.HasPrefix(resp, "2") {
		log.Println("Error uploading class:", resp, string(body))
		return false
	}
	return true
}

type Schema TypeSafeSchema

func CreateClass(class, vectorizer string) {

	newClass := TypeSafeClass{Class: class}
	newClass.Description = "Auto generated class"
	newClass.InvertedIndexConfig = &models.InvertedIndexConfig{}
	newClass.InvertedIndexConfig.Bm25 = &models.BM25Config{}
	newClass.InvertedIndexConfig.Bm25.B = 0.75
	newClass.InvertedIndexConfig.Bm25.K1 = 1.2
	newClass.InvertedIndexConfig.CleanupIntervalSeconds = 60
	newClass.InvertedIndexConfig.Stopwords = &models.StopwordConfig{}
	newClass.InvertedIndexConfig.Stopwords.Preset = "en"
	newClass.Vectorizer = vectorizer

	UploadClass(newClass)

}

// ChatGPT api

//Response: {"object":"list","model":"text-embedding-ada-002","data":[{"embedding":[],"index":0,"object":"embedding"}]

type EmbeddingRequest struct {
	Input string `json:"input"`
	Model string `json:"model"`
}

type EmbeddingResponse struct {
	Model  string `json:"model"`
	Object string `json:"object"`
	Data   []struct {
		Embedding []float32 `json:"embedding"`
		Index     int       `json:"index"`
		Object    string    `json:"object"`
	} `json:"data"`
}

func GetEmbedding(input, model, apiKey, serverAddress string, retries int) ([]float32, error) {
	if serverAddress == "" {
		serverAddress = "https://api.openai.com"
	}

	url := fmt.Sprintf("%s/v1/embeddings", serverAddress)

	// Create request payload
	payload := EmbeddingRequest{
		Input: input,
		Model: model,
	}

	fmt.Printf("Getting embedding for: %v\n", payload)
	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}

	// Create new request
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonPayload))
	if err != nil {
		return nil, err
	}

	// Add headers
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", apiKey))

	// Send the request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// Read and decode the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	fmt.Printf("Response: %v\n", string(body))

	var embeddingResponse EmbeddingResponse
	err = json.Unmarshal(body, &embeddingResponse)
	if err != nil {
		return nil, err
	}

	if len(embeddingResponse.Data) == 0 {
		if retries > 0 {
			fmt.Printf("Retrying %v more times\n", retries)
			time.Sleep(1 * time.Second)
			return GetEmbedding(input, model, apiKey, serverAddress, retries-1)
		} else {
			return nil, fmt.Errorf("no embedding found")
		}
	} else {
		return embeddingResponse.Data[0].Embedding, nil
	}
}

func VectorSearch(class, search string, limit int) []map[string]string {
	nullresult := []map[string]string{}

	properties := GetProperties_str(class)
	query := fmt.Sprintf(`{"query":"{Get {%v (limit: %v, nearText: { concepts: [\"%v\"] }){%v _additional {score vector id}}}}","variables":{}}`, class, limit, search, properties) //fixme remove hardcoded title

	logError("GraphQL query: %v\n\n", query)

	url := WeaviateUrl + "graphql"
	resp, err := http.Post(url, "application/json", bytes.NewBuffer([]byte(query)))
	if err != nil {
		log.Println(err)
		return nil
	}
	defer resp.Body.Close()
	// Print the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return nullresult
	}
	//log.Println(string(body))

	// Unmarshal the response
	var result resultFlexi
	err = json.Unmarshal(body, &result)

	if err != nil {
		log.Println(err)
		return nullresult
	}

	log.Println(result.Data)

	return UnpackResults(result)
}

func VectorSearchMap(class, search string, limit int) []map[string]interface{} {
	nullresult := []map[string]interface{}{}

	/*
		vector, err := GetEmbedding(search, "text-embedding-ada-002", os.Getenv("OPENAI_API_KEY"), "http://192.168.11.25:8080",10)
		if err != nil {
			log.Println("Could not get embedding", err)
			return nullresult
		}

		//convert vector to string, like [ 0.1, 0.2, 0.3 ]
		vector_str := "["
		for _, v := range vector {
			vector_str += fmt.Sprintf("%v, ", v)
		}
		vector_str = vector_str[:len(vector_str)-2] + "]"
	*/

	properties := GetProperties_str(class)
	query := fmt.Sprintf(`{"query":"{Get {%v (limit: %v, nearText: { concepts: [\"%v\"] }){%v _additional {score vector id}}}}","variables":{}}`, class, limit, search, properties) //fixme remove hardcoded title

	logError("GraphQL query: %v\n\n", query)

	url := WeaviateUrl + "graphql"
	resp, err := http.Post(url, "application/json", bytes.NewBuffer([]byte(query)))
	if err != nil {
		log.Println(err)
		return nil
	}
	defer resp.Body.Close()
	// Print the response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return nullresult
	}
	//log.Println(string(body))

	// Unmarshal the response
	var result resultFlexi
	err = json.Unmarshal(body, &result)

	if err != nil {
		log.Println(err)
		return nullresult
	}

	log.Println(result.Data)

	return UnpackResultsInterface(result)
}

func CreateGrpcWeaviateClient(conn *grpc.ClientConn) pb.WeaviateClient {
	return pb.NewWeaviateClient(conn)
}

func CreateGrpcConnectionClient(host string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithBlock())
	if strings.HasSuffix(host, ":443") {
		tlsConfig := &tls.Config{
			InsecureSkipVerify: true,
		}
		opts = append(opts, grpc.WithTransportCredentials(credentials.NewTLS(tlsConfig)))
	} else {
		opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	}
	conn, err := grpc.Dial(host, opts...)
	if err != nil {
		return nil, fmt.Errorf("failed to dial: %w", err)
	}
	return conn, nil
}

func GrpcHybrid(tenant string, class string, query string, limit int) []string {
	conn, _ := CreateGrpcConnectionClient(":50051")
	grpcClient := CreateGrpcWeaviateClient(conn)
	req := &pb.SearchRequest{
		Collection: class,
		HybridSearch: &pb.Hybrid{
			Query:      query,
			Properties: GetProperties(class),
		},

		Properties: &pb.PropertiesRequest{
			NonRefProperties: GetProperties(class),
		},
		Limit: uint32(limit),

		Metadata: &pb.MetadataRequest{
			Uuid: true,
		},
	}
	if tenant != "" {
		req.Tenant = tenant
	}
	fmt.Printf("Request: %+v\n", req)
	resp, err := grpcClient.Search(context.TODO(), req)
	if err != nil {
		log.Println(err)
		return nil
	}
	out := []string{}
	for _, obj := range resp.Results {
		fields := obj.Properties.NonRefProperties.Fields
		res := ""
		for fieldName, _ := range fields {
			res += fmt.Sprintf("%v: %v\n", fieldName, obj.Properties.String())
		}

		out = append(out, res)
	}
	return out
}

func GrpcHybridMap(tenant string, class string, query string, limit int) []map[string]interface{} {
	conn, _ := CreateGrpcConnectionClient(":50051")
	grpcClient := CreateGrpcWeaviateClient(conn)
	req := &pb.SearchRequest{
		Collection: class,
		HybridSearch: &pb.Hybrid{
			Query:      query,
			Properties: GetProperties(class),
		},

		Properties: &pb.PropertiesRequest{
			NonRefProperties: GetProperties(class),
		},
		Limit: uint32(limit),

		Metadata: &pb.MetadataRequest{
			Uuid: true,
		},
	}
	if tenant != "" {
		req.Tenant = tenant
	}
	fmt.Printf("Request: %+v\n", req)
	resp, err := grpcClient.Search(context.TODO(), req)
	if err != nil {
		log.Println(err)
		return nil
	}
	out := []map[string]interface{}{}
	for _, obj := range resp.Results {
		fields := obj.Properties.NonRefProperties.Fields
		res := make(map[string]interface{})
		for fieldName, ff := range fields {
			res[fieldName] = ff.AsInterface()
		}

		out = append(out, res)
	}
	return out
}

func GrpcBM25(tenant, class string, query []string, limit int) []map[string]string {
	conn, _ := CreateGrpcConnectionClient(":50051")
	grpcClient := CreateGrpcWeaviateClient(conn)
	req := &pb.SearchRequest{
		Collection: class,
		Bm25Search: &pb.BM25{
			Tokens: query,
		},

		Properties: &pb.PropertiesRequest{
			NonRefProperties: GetProperties(class),
		},
		Limit: uint32(limit),

		Metadata: &pb.MetadataRequest{
			Uuid: true,
		},
	}
	if tenant != "" {
		req.Tenant = tenant
	}
	fmt.Printf("GrpcBM25 Request: %+v\n", req)
	resp, err := grpcClient.Search(context.TODO(), req)
	if err != nil {
		log.Println(err)
		return nil
	}

	jsonText, _ := json.Marshal(resp)
	fmt.Printf("GrpcBM25 Response: %+v\n", string(jsonText))

	out := []map[string]string{}

	for _, obj := range resp.Results {

		o := make(map[string]string)
		if obj == nil {
			panic("nil object")
		}
		if obj.Properties == nil {
			panic("nil properties")
		}

		if obj.Properties.TextArrayProperties != nil {
			fields := obj.Properties.TextArrayProperties
			for _, field := range fields {
				fieldName := field.PropName
				fieldValue := field.Values
				o[fieldName] = fmt.Sprintf("%v", fieldValue)
			}
		}

		if obj.Properties.IntArrayProperties != nil {
			fields := obj.Properties.IntArrayProperties
			for _, field := range fields {
				fieldName := field.PropName
				fieldValue := field.Values
				o[fieldName] = fmt.Sprintf("%v", fieldValue)
			}
		}

		if obj.Properties.NumberArrayProperties != nil {
			fields := obj.Properties.NumberArrayProperties
			for _, field := range fields {
				fieldName := field.PropName
				fieldValue := field.Values
				o[fieldName] = fmt.Sprintf("%v", fieldValue)
			}
		}

		if obj.Properties.BooleanArrayProperties != nil {
			fields := obj.Properties.BooleanArrayProperties
			for _, field := range fields {
				fieldName := field.PropName
				fieldValue := field.Values
				o[fieldName] = fmt.Sprintf("%v", fieldValue)
			}
		}

		if obj.Properties.NonRefProperties != nil {
			fields := obj.Properties.NonRefProperties.Fields

			for fieldName, val := range fields {
				o[fieldName] = fmt.Sprintf("%v", val.AsInterface())
			}
		}
		out = append(out, o)
	}
	return out
}


func GrpcInsertExampleObject(tenant, className, id string, tokens []string, exampleText string) {
	conn, _ := CreateGrpcConnectionClient(":50051")
	grpcClient := CreateGrpcWeaviateClient(conn)
	id1 := uuid.New()
	id2 := uuid.New()
	id3 := uuid.New()
	id4 := uuid.New()

	req := &pb.BatchObjectsRequest{

		Objects: []*pb.BatchObject{
			{
				Tokens: []string{
					"Dampf", "schiff", "fahrts", "fahrt", "fahren",
					"gesellschafts", "gesellschaft",
					"direktions", "assistenten",
					"stellvertreter",
				},
				Collection: className,
				Uuid:       id1.String(),
				Properties: &pb.BatchObject_Properties{
					NonRefProperties: &structpb.Struct{
						Fields: map[string]*structpb.Value{
							"title":       structpb.NewStringValue("Compound nouns"),
							"description": structpb.NewStringValue("Dampfschifffahrtsgesellschaftsdirektionsassistentenstellvertreter"),
						},
					},
					ObjectProperties: []*pb.ObjectProperties{{
						PropName: "meta",
						Value: &pb.ObjectPropertiesValue{
							NonRefProperties: &structpb.Struct{Fields: map[string]*structpb.Value{"isbn": structpb.NewStringValue("978-0593099322")}},
							ObjectProperties: []*pb.ObjectProperties{{
								PropName: "obj",
								Value: &pb.ObjectPropertiesValue{
									NonRefProperties: &structpb.Struct{Fields: map[string]*structpb.Value{"text": structpb.NewStringValue("some text")}},
								},
							}},
							ObjectArrayProperties: []*pb.ObjectArrayProperties{{
								PropName: "objs",
								Values: []*pb.ObjectPropertiesValue{{
									NonRefProperties: &structpb.Struct{Fields: map[string]*structpb.Value{"text": structpb.NewStringValue("some text")}},
								}},
							}},
						},
					}},
					ObjectArrayProperties: []*pb.ObjectArrayProperties{{
						PropName: "reviews",
						Values:   []*pb.ObjectPropertiesValue{{TextArrayProperties: []*pb.TextArrayProperties{{PropName: "tags", Values: []string{"scifi", "epic"}}}}},
					}},
				},
			},
			{
				Tokens: []string{"aufstehen", "auf", "stehen",
    "anziehen", "an", "ziehen",
    "einsteigen", "ein", "steigen",
    "aussteigen", "aus", "steigen",
    "umsteigen", "um", "steigen",
    "abfahren", "ab", "fahren",
    "ankommen", "an", "kommen",
    "abholen", "ab", "holen",
    "mitnehmen", "mit", "nehmen",
    "auspacken", "aus", "packen",
    "einpacken", "ein", "packen",
    "umtauschen", "um", "tauschen",
    "einladen", "ein", "laden",
    "ausladen", "aus", "laden"},
				Collection: className,
				Uuid:       id2.String(),
				Properties: &pb.BatchObject_Properties{
					NonRefProperties: &structpb.Struct{
						Fields: map[string]*structpb.Value{
							"title":       structpb.NewStringValue("separable verbs"),
							"description": structpb.NewStringValue("aufstehen, anziehen, einsteigen, aussteigen, umsteigen, abfahren, ankommen, abholen, mitnehmen, auspacken, einpacken, umtauschen, einladen, ausladen, einsteigen, aussteigen, umsteigen, abfahren, ankommen, abholen, mitnehmen, auspacken, einpacken, umtauschen, einladen, ausladen"),
						},
					},
					ObjectProperties: []*pb.ObjectProperties{{
						PropName: "meta",
						Value: &pb.ObjectPropertiesValue{
							NonRefProperties: &structpb.Struct{Fields: map[string]*structpb.Value{"isbn": structpb.NewStringValue("978-0593135204")}},
							ObjectProperties: []*pb.ObjectProperties{{
								PropName: "obj",
								Value:    &pb.ObjectPropertiesValue{NonRefProperties: &structpb.Struct{Fields: map[string]*structpb.Value{"text": structpb.NewStringValue("some text")}}},
							}},
							ObjectArrayProperties: []*pb.ObjectArrayProperties{{
								PropName: "objs",
								Values: []*pb.ObjectPropertiesValue{{
									NonRefProperties: &structpb.Struct{Fields: map[string]*structpb.Value{"text": structpb.NewStringValue("some text")}},
								}},
							}},
						},
					}},
					ObjectArrayProperties: []*pb.ObjectArrayProperties{{
						PropName: "reviews",
						Values:   []*pb.ObjectPropertiesValue{{TextArrayProperties: []*pb.TextArrayProperties{{PropName: "tags", Values: []string{"scifi"}}}}},
					}},
				},
			},
			{
				Tokens:  []string{
					"カイル", "は", "カエル", "を", "買える", "か",
					"カエル", "に", "変わる", "か",
					"それとも", "殻", "に", "帰る", "か",
				},
				Collection: className,
				Uuid:       id3.String(),
				Properties: &pb.BatchObject_Properties{
					NonRefProperties: &structpb.Struct{
						Fields: map[string]*structpb.Value{
							"title":       structpb.NewStringValue("Japanese"),
							"description": structpb.NewStringValue(`カイルはカエルを買えるか、カエルに変わるか、それとも殻に帰るか？`),
						},
					},
					ObjectProperties: []*pb.ObjectProperties{{
						PropName: "meta",
						Value: &pb.ObjectPropertiesValue{
							NonRefProperties: &structpb.Struct{Fields: map[string]*structpb.Value{"isbn": structpb.NewStringValue("978-8374812962")}},
							ObjectProperties: []*pb.ObjectProperties{{
								PropName: "obj",
								Value:    &pb.ObjectPropertiesValue{NonRefProperties: &structpb.Struct{Fields: map[string]*structpb.Value{"text": structpb.NewStringValue("some text")}}},
							}},
							ObjectArrayProperties: []*pb.ObjectArrayProperties{{
								PropName: "objs",
								Values: []*pb.ObjectPropertiesValue{{
									NonRefProperties: &structpb.Struct{Fields: map[string]*structpb.Value{"text": structpb.NewStringValue("some text")}},
								}},
							}},
						},
					}},
					ObjectArrayProperties: []*pb.ObjectArrayProperties{{
						PropName: "reviews",
						Values:   []*pb.ObjectPropertiesValue{{TextArrayProperties: []*pb.TextArrayProperties{{PropName: "tags", Values: []string{"scifi", "fantasy"}}}}},
					}},
				},
			},

			{
				Tokens:  []string{
					"施氏", "食", "狮", "史",
					"石室", "诗士", "施氏", "嗜", "狮", "誓", "食", "十", "狮",
					"氏", "时时", "适", "市", "视", "狮",
					"十时", "适", "十", "狮", "适", "市",
					"是时", "适", "施氏", "适", "市",
					"氏", "视", "是", "十", "狮", "恃", "矢", "势", "使", "是", "十", "狮", "逝世",
					"氏", "拾", "是", "十", "狮", "尸", "适", "石室",
					"石室", "湿", "氏", "使", "侍", "拭", "石室",
					"石室", "拭", "氏", "始", "试", "食", "是", "十", "狮", "尸",
					"食时", "始", "识", "是", "十", "狮", "尸", "实", "十", "石", "狮", "尸",
					"试", "释", "是", "事",
				},
				Collection: className,
				Uuid:       id4.String(),
				Properties: &pb.BatchObject_Properties{
					NonRefProperties: &structpb.Struct{
						Fields: map[string]*structpb.Value{
							"title":       structpb.NewStringValue("Chinese"),
							"description": structpb.NewStringValue(`施氏食狮史
石室诗士施氏，嗜狮，誓食十狮。
氏时时适市视狮。
十时，适十狮适市。
是时，适施氏适市。
氏视是十狮，恃矢势，使是十狮逝世。
氏拾是十狮尸，适石室。
石室湿，氏使侍拭石室。
石室拭，氏始试食是十狮尸。
食时，始识是十狮尸，实十石狮尸。
试释是事。`),
						},
					},
					ObjectProperties: []*pb.ObjectProperties{{
						PropName: "meta",
						Value: &pb.ObjectPropertiesValue{
							NonRefProperties: &structpb.Struct{Fields: map[string]*structpb.Value{"isbn": structpb.NewStringValue("978-8374812962")}},
							ObjectProperties: []*pb.ObjectProperties{{
								PropName: "obj",
								Value:    &pb.ObjectPropertiesValue{NonRefProperties: &structpb.Struct{Fields: map[string]*structpb.Value{"text": structpb.NewStringValue("some text")}}},
							}},
							ObjectArrayProperties: []*pb.ObjectArrayProperties{{
								PropName: "objs",
								Values: []*pb.ObjectPropertiesValue{{
									NonRefProperties: &structpb.Struct{Fields: map[string]*structpb.Value{"text": structpb.NewStringValue("some text")}},
								}},
							}},
						},
					}},
					ObjectArrayProperties: []*pb.ObjectArrayProperties{{
						PropName: "reviews",
						Values:   []*pb.ObjectPropertiesValue{{TextArrayProperties: []*pb.TextArrayProperties{{PropName: "tags", Values: []string{"scifi", "fantasy"}}}}},
					}},
				},
			},
		},
	}

	

	fmt.Printf("Request: %+v\n", req)
	resp, err := grpcClient.BatchObjects(context.TODO(), req)
	if err != nil {
		log.Println(err)
	}
	fmt.Printf("Response: %+v\n", resp)
}

func TenantSearch(tenant string, class string, query string, limit int) []string {
	conn, _ := CreateGrpcConnectionClient(":50051")
	grpcClient := CreateGrpcWeaviateClient(conn)
	req := &pb.SearchRequest{
		Collection: class,
		Tenant:     tenant,
		HybridSearch: &pb.Hybrid{
			Query:      query,
			Properties: GetProperties(class),
		},

		Properties: &pb.PropertiesRequest{
			NonRefProperties: GetProperties(class),
		},
		Limit: uint32(limit),

		Metadata: &pb.MetadataRequest{
			Uuid: true,
		},
	}
	fmt.Printf("Request: %+v\n", req)
	resp, err := grpcClient.Search(context.TODO(), req)
	if err != nil {
		log.Println(err)
		return nil
	}
	out := []string{}
	for _, obj := range resp.Results {
		fields := obj.Properties.NonRefProperties.Fields
		res := ""
		for fieldName, _ := range fields {
			res += fmt.Sprintf("%v: %v\n", fieldName, obj.Properties.String())
		}

		out = append(out, res)
	}
	return out
}
