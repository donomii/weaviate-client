package main

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"


	"flag"
	"io/ioutil"
	"math/rand"
	"strconv"

	"github.com/google/uuid"
	"github.com/chzyer/readline"
	"github.com/donomii/goof"
	"github.com/mattn/go-shellwords"
	"github.com/pjovanovic05/gojq"
	weaviate "gitlab.com/donomii/weaviate-client"

)

var shortResults = false
var Page = 0

type Settings_s struct {
	ResultLengthLimit int
	ResultLimit       int
}

func CompletePropertiesForBm25() func(string) []string {
	return func(line string) []string {
		// Split line into words
		words, err := shellwords.Parse(line)
		if err != nil {
			fmt.Println("Error parsing line", err)
			return nil
		}
		if len(words) < 2 {
			return nil
		}
		// Get class
		class := words[1]
		// Get properties
		properties := weaviate.GetProperties(class)
		return properties
	}
}

var Settings Settings_s
var completer = readline.NewPrefixCompleter(
	readline.PcItem("mode",
		readline.PcItem("vi"),
		readline.PcItem("emacs"),
	),
	readline.PcItem("list",
		readline.PcItem("classes"),
		readline.PcItem("shards", readline.PcItemDynamic(weaviate.CompleteClasses())),
		readline.PcItem("tenants", readline.PcItemDynamic(weaviate.CompleteClasses())),
		readline.PcItem("objects", readline.PcItemDynamic(weaviate.CompleteClasses())),
	),
	readline.PcItem("randomClass"),
	readline.PcItem("show",
		readline.PcItem("class", readline.PcItemDynamic(weaviate.CompleteClasses())),
		readline.PcItem("schema"),
		readline.PcItem("object"),
	),
	readline.PcItem("upload", readline.PcItem("schema")),
	readline.PcItem("download", readline.PcItem("schema")),
	readline.PcItem("url"),
	readline.PcItem("delete",
		readline.PcItem("class", readline.PcItemDynamic(weaviate.CompleteClasses())),
		readline.PcItem("shard", readline.PcItemDynamic(weaviate.CompleteClasses())),
		readline.PcItem("object", readline.PcItemDynamic(weaviate.CompleteClasses())),
	),
	readline.PcItem("search", readline.PcItemDynamic(weaviate.CompleteClasses())),
	readline.PcItem("tenantsearch", readline.PcItemDynamic(weaviate.CompleteClasses())),
	readline.PcItem("vector", readline.PcItem("search", readline.PcItemDynamic(weaviate.CompleteClasses()))),
	readline.PcItem("bm25", readline.PcItemDynamic(weaviate.CompleteClasses())),
	readline.PcItem("filter",
		readline.PcItemDynamic(weaviate.CompleteClasses(),
			readline.PcItem("Like", readline.PcItemDynamic(CompletePropertiesForBm25())),
			readline.PcItem("NotLike", readline.PcItemDynamic(CompletePropertiesForBm25())),
			readline.PcItem("Equal", readline.PcItemDynamic(CompletePropertiesForBm25())),
		),
	),
	readline.PcItem("set",
		readline.PcItem("ResultLengthLimit"),
		readline.PcItem("ShortResults"),
	),
	readline.PcItem("dump", readline.PcItem("to")),
	readline.PcItem("create",
		readline.PcItem("object", readline.PcItemDynamic(weaviate.CompleteClasses())),
		readline.PcItem("class",
			readline.PcItem("className"),
			readline.PcItem("vectoriser",
				readline.PcItem("text2vec-contextionary"),
				readline.PcItem("text2vec-openai"),
				readline.PcItem("text2vec-transformers"),
				readline.PcItem("multi2vec-clip"),
				readline.PcItem("text2vec-huggingface"),
				readline.PcItem("none"),
				readline.PcItem("text2vec-cohere"),
			),
			readline.PcItem("model",
				readline.PcItem("ada"),
				readline.PcItem("babbage"),
				readline.PcItem("curie"),
				readline.PcItem("davinci"),
			),
			readline.PcItem("version",
				readline.PcItem("001"),
				readline.PcItem("002"),
				readline.PcItem("003"),
			),
			readline.PcItem("type",
				readline.PcItem("text"),
				readline.PcItem("code"),
			),
		),
	),
	readline.PcItem("nodes"),
	readline.PcItem("settings"),
	readline.PcItem("ask"),
	readline.PcItem("20news"),
	readline.PcItem("schema", readline.PcItem("set")),
	readline.PcItem("grpchybrid", readline.PcItemDynamic(weaviate.CompleteClasses())),
	readline.PcItem("grpcbm25", readline.PcItemDynamic(weaviate.CompleteClasses())),
	readline.PcItem("info"),
	readline.PcItem("exit"),
	readline.PcItem("help"),
)

type DbDump struct {
	Objects map[string]map[string]interface{}
	Schema  weaviate.Schema
}

func main() {
	var command string
	flag.StringVar(&weaviate.WeaviateUrl, "url", "http://localhost:8080/v1/", "Weaviate URL")
	flag.StringVar(&command, "command", "", "Command to run")
	flag.Parse()

	// Display the banner upon startup
	DisplayBanner()

	if command != "" {
		processCommand(command)
		return
	}

	// Create a custom prompt with nice styling
	promptText := promptStyle.Render(fmt.Sprintf("(%s) > ", weaviate.WeaviateUrl))
	
	l, err := readline.NewEx(&readline.Config{
		Prompt:          promptText,
		HistoryFile:     "/tmp/readline.tmp",
		AutoComplete:    completer,
		InterruptPrompt: "^C",
		EOFPrompt:       "exit",
	})
	if err != nil {
		panic(err)
	}
	defer l.Close()

	for {
		line, err := l.Readline()
		if err != nil { // io.EOF
			break
		}

		processCommand(line)
		
		// Update prompt in case URL changed
		l.SetPrompt(promptStyle.Render(fmt.Sprintf("(%s) > ", weaviate.WeaviateUrl)))
	}
}

func processCommand(line string) {

	// Split line into words
	words, err := shellwords.Parse(line)
	if err != nil {
		fmt.Println("Error parsing line", err)
		return
	}

	if len(words) == 0 {
		weaviate.Probe()
		return
	}

	useJSON := false
	if words[len(words)-1] == "-json" {
		useJSON = true
		words = words[:len(words)-1]
	}

	// Check if the first word is a command
	switch words[0] {
	case "set":
		if len(words) > 2 {
			switch words[1] {
			case "ResultLengthLimit":
				Settings.ResultLengthLimit, _ = strconv.Atoi(words[2])
			case "ResultLimit":
				Settings.ResultLimit, _ = strconv.Atoi(words[2])
			case "ShortResults":
				if words[2] == "true" {
					shortResults = true
				} else {
					shortResults = false
				}
			default:
				fmt.Println("Unknown setting", words[1])
			}
		}
	case "exit":
		os.Exit(0)
	case "url":
		if len(words) == 2 {
			weaviate.WeaviateUrl = words[1]
		} else {
			fmt.Println(weaviate.WeaviateUrl)
		}
	case "delete":

		if len(words) > 1 {
			switch words[1] {
			case "class":
				// Make REST call
				resp, body := weaviate.DoRESTCall("DELETE", weaviate.WeaviateUrl+"schema/"+words[2], nil)
				fmt.Println("Delete:", resp, string(body))
			case "shard":
				// Make REST call
				weaviate.DoRESTCall("DELETE", weaviate.WeaviateUrl+"schema/"+words[1]+"/shards/"+words[2], nil)
			case "object":

			}
		} else {
			fmt.Println("Unknown args")
		}
	case "search":
		if len(words) > 2 {
			class := words[1]

			results := weaviate.HybridPropResults(words[2:], class, 10)
			fmt.Println("Results", results)
			FormatResults(results, useJSON)
		} else {
			fmt.Println("search requires at least one argument")
		}
	case "tenantsearch":
		if len(words) > 2 {
			class := words[1]
			tenant := words[2]
			results := weaviate.TenantSearch(tenant, class, strings.Join(words[3:], " "), 10)
			if len(results) == 0 {
				fmt.Println("No results found")
			} else {
				// Print results
				for _, result := range results {
					fmt.Println(result)
				}
			}
		}
	case "grpchybrid":
		if len(words) > 2 {
			class := words[1]
			results := weaviate.GrpcHybrid("", class, strings.Join(words[3:], " "), 10)
			if len(results) == 0 {
				fmt.Println("No results found")
			} else {
				// Print results
				for _, result := range results {
					fmt.Println(result)
				}
			}
		}
	case "grpcbm25":
		if len(words) > 1 {
			class := words[1]
			results := weaviate.GrpcBM25("", class, words[2:], 10)
			if len(results) == 0 {
				fmt.Println("No results found")
			} else {
				fmt.Printf("%v results found\n", len(results))
				FormatResults(results, useJSON)
			}
		}
	case "bm25":
		if len(words) > 2 {
			class := words[1]
			var results []map[string]string

			fmt.Println("Searching for", strings.Join(words[2:], " "), "in class", class)
			results = weaviate.BM25fPropResults(strings.Join(words[2:], " "), class, 10)

			if len(results) == 0 {
				fmt.Println("No results found")
			} else {
				// Print results
				fmt.Println("Printing results")
				for _, result := range results {
					if shortResults {
						fmt.Printf("Id: %v, Score: %v\n", result["DocId"], result["Score"])
					} else {
						//Print each key, value pair, with the value trimmed to 1024 characters
						for key, value := range result {
							valstr := fmt.Sprintf("%v", value)
							if len(valstr) > 1024 {
								valstr = valstr[:1024]
							}
							fmt.Printf("%v: %v\n", key, valstr)
						}
					}
				}
			}

		} else {
			fmt.Println("bm25 requires at least one argument")
		}
	case "filter":
		if len(words) > 3 {
			class := words[1]
			var results []map[string]string

			operator := words[2]
			path := words[3]
			query := strings.Join(words[4:], " ")
			filters := []weaviate.Filter{
				{Path: path, Operator: operator, Value: query},
			}

			results = weaviate.BM25fPropResultsWithFilter(class, path, 10, filters...)
			if len(results) == 0 {
				fmt.Println("No results found")
			} else {
				// Print results
				for _, result := range results {
					if shortResults {
						fmt.Printf("Id: %v, Score: %v\n", result["DocId"], result["Score"])
					} else {
						//Print each key, value pair, with the value trimmed to 1024 characters
						for key, value := range result {
							valstr := fmt.Sprintf("%v", value)
							if len(valstr) > 1024 {
								valstr = valstr[:1024]
							}
							fmt.Printf("%v: %v\n", key, valstr)
						}
					}
				}
			}
		} else {
			fmt.Println("filter requires at least one argument")
		}
	case "inserttest":
		if len(words) == 2 {
			class := words[1]
			// Make a uuid
			id := uuid.New()
			weaviate.GrpcInsertExampleObject("", class, id.String(), []string{"test", "test2", "test3"}, "lalalalalala")
		} else {
			fmt.Println("inserttest requires class: and id:")
		}
	case "ask":
		if len(words) > 1 {
			fmt.Printf("Calling AskQuestion with %v, %v\n", words[1], strings.Join(words[2:], " "))
			results := weaviate.AskQuestion(words[1], strings.Join(words[2:], " "), 10)
			// Print results
			for _, result := range results {
				fmt.Println(result)
			}
		} else {
			fmt.Println("ask requires at least one argument")
		}
		/*
			case "generative":
				data ,_:= json.Marshal(words)
				fmt.Printf("generative: %v\n", string(data))
				if len(words) > 2 {
					fmt.Printf("Calling Generative with %v, %v\n", words[1], strings.Join(words[2:], " "))
					results := weaviate.Generative(words[1],strings.Join(words[2:], " "),10)
					// Print results
					fmt.Println(results)
				} else {
					fmt.Println("generative requires at least two arguments")
				}
		*/
	case "list":
		switch words[1] {
		case "classes":
			if len(words) == 2 && words[1] == "classes" {
				classes := weaviate.GetClasses()
				for _, class := range classes {
					fmt.Println(class)
				}
			}
		case "tenants":
			if len(words) == 3 && words[1] == "tenants" {
				class := words[2]
				tenants := weaviate.GetTenants(class)
				for _, tenant := range tenants {
					fmt.Println(tenant)
				}
			}
		case "shards":
			if len(words) == 3 && words[1] == "shards" {
				// Make REST call
				shards := weaviate.GetShards(words[2])
				for _, shard := range shards {
					fmt.Println(shard)
				}
			} else {
				fmt.Println("list shards requires a single argument")
			}

		case "objects":
			if len(words) == 3 && words[1] == "objects" {
				fmt.Printf("Objects:\n")
				for _, object := range weaviate.GetObjects(words[2], -1) {
					//Pretty-print the object
					b, err := json.MarshalIndent(object, "", "  ")
					if err != nil {
						fmt.Println("error:", err)
					}
					fmt.Println(string(b))
				}
			}
		}
	case "dump":
		if len(words) != 3 {
			fmt.Println("dump to requires a file name")
			return
		}
		filename := words[2]
		//Dumps db to disk
		db := DbDump{Schema: weaviate.GetSchema(), Objects: map[string]map[string]interface{}{}}
		count := 0
		//Iterate over all classes
		for _, class := range weaviate.GetClasses() {
			//Iterate over all objects
			for _, object := range weaviate.GetObjects(class, -1) {
				classm := db.Objects[class]
				if classm == nil {
					classm = map[string]interface{}{}
				}
				//Add object to db
				classm[object["DocId"]] = object
				db.Objects[class] = classm
				count++
			}
		}

		// Marshal db to json and write to filename
		b, err := json.MarshalIndent(db, "", "  ")
		if err != nil {
			fmt.Println("error:", err)
		}

		ioutil.WriteFile(filename, b, 0644)

		fmt.Println("Dumped", count, "objects to", filename)

	case "show":
		switch len(words) {
		case 3:
			if words[1] == "class" {
				// Make REST call
				_, body := weaviate.DoRESTCall("GET", weaviate.WeaviateUrl+"schema/"+words[2], nil)
				//Unpack into weaviate.Schema
				var schema weaviate.TypeSafeClass
				err := json.Unmarshal(body, &schema)
				if err != nil {
					fmt.Println("error:", err)
				}
				//Pretty print
				b, err := json.MarshalIndent(schema, "", "  ")

				fmt.Println(string(b))
			}
			if words[1] == "object" {
				obj := weaviate.GetObject(words[2])
				//Pretty print
				b, err := json.MarshalIndent(obj, "", "  ")
				if err != nil {
					fmt.Println("error:", err)
				}
				fmt.Println(string(b))
			}
		case 2:
			if words[1] == "schema" {
				s := weaviate.GetSchema()
				//Pretty print
				b, err := json.MarshalIndent(s, "", "  ")
				if err != nil {
					fmt.Println("error:", err)
				}
				fmt.Println(string(b))

			}
		}

	case "info":
		weaviate.Info()
	case "20news":
		fmt.Println("Downloading 20news repository")
		fmt.Println(goof.Shell("git clone git@github.com:semi-technologies/20news-classification 20news"))
		fmt.Println("Switching directory ")
		os.Chdir("20news")
		fmt.Println("Downloading dataset")
		goof.Shell("./download.sh")
		fmt.Println("Importing dataset")
		goof.Shell("go run import.go")
		fmt.Println("Switching directory ")
		os.Chdir("..")
	case "help":
		help()
	case "upload":
		if len(words) == 3 {
			if words[1] == "schema" {
				weaviate.UploadSchemaFromFile(words[2])
			}
		}
	case "download":
		if len(words) == 3 {
			if words[1] == "schema" {
				s := weaviate.GetSchema()
				//Pretty print
				b, err := json.MarshalIndent(s, "", "  ")
				if err != nil {
					fmt.Println("error:", err)
				}
				//Save b to file
				err = ioutil.WriteFile(words[2], b, 0644)
				if err != nil {
					fmt.Println("error:", err)
				}
			}
		}
	case "nodes":
		weaviate.Nodes()
	case "create":

		switch words[1] {
		case "object":
			class := words[2]
			// Read property value combinations from words
			properties := map[string]interface{}{}
			for i := 3; i < len(words); i = i + 2 {
				// property and value are in even and odd positions in words

				// property
				property := words[i]
				fmt.Println("property", property)
				// value
				value := words[i+1]
				// Add to properties
				properties[property] = value

			}
			// Create object
			objWrapper := weaviate.ObjectsInsert{}
			objs := []weaviate.ObjectInsert{}
			obj := weaviate.ObjectInsert{}
			obj.Class = class
			obj.Properties = map[string]interface{}{}
			for prop, val := range properties {

				obj.Properties[prop] = val
			}
			objs = append(objs, obj)
			objWrapper.Objects = objs
			weaviate.InsertObjects(objWrapper)

		case "class":
			if len(words) != 4 {
				fmt.Println("create class requires 4 arguments: create class [classname] [vectoriser]")
				return
			}
			class := words[2]
			vectoriser := words[3]

			weaviate.CreateClass(class, vectoriser)
		}

	case "schema":
		if words[1] == "set" {
			s := weaviate.GetRawSchema()
			jq := gojq.FromBytes([]byte(s))
			path := words[2 : len(words)-1]
			value := words[len(words)-1]
			fmt.Println("path", path)
			fmt.Println("value", value)

			// Get the value from a deeply nested property
			var currentVal string
			jq.Select(path...).As(&currentVal)
			if jq.Err != nil {
				fmt.Printf("Could not find path %v, %v\n", path, jq.Err)
				return
			}
			fmt.Println("The current value is:", currentVal)

			// Set a value
			currentVal = value
			jq.Select(path...).Set(currentVal)

			jq.Select(path...).As(&currentVal)
			if jq.Err != nil {
				fmt.Printf("Could not find path %v, %v\n", path, jq.Err)
				return
			}
			fmt.Println("The new value is:", currentVal)

			weaviate.SetSchema([]byte(jq.String()))

		}

	case "randomClass":
		// Create a class called "RandomClass"
		// with a property called "randomProperty"

		for i := 0; i < 100; i++ {
			// Create a random string
			val1 := make([]byte, 10)
			rand.Read(val1)
			val2 := make([]byte, 10)
			rand.Read(val2)
			// Create a random vector of length []float32
			randomVec := make([]float32, 300)
			for i := range randomVec {
				randomVec[i] = rand.Float32()
			}
			objs := weaviate.ObjectsInsert{
				Objects: []weaviate.ObjectInsert{
					{
						Class: "RandomClass",
						Properties: map[string]interface{}{
							"randomprop1": "frog " + string(val1),
							"randomprop2": "chicken" + string(val2),
						},
						Vector: randomVec,
					},
				},
			}
			weaviate.InsertObjects(objs)
		}
	case "vector":
		class := words[2]
		searchWords := words[3:]
		searchText := strings.Join(searchWords, " ")
		results := weaviate.VectorSearch(class, searchText, 10)
		if len(results) == 0 {
			fmt.Println("No results found")
		} else {
			// Print results
			for _, result := range results {
				if shortResults {
					fmt.Printf("Id: %v, Score: %v\n", result["DocId"], result["Score"])
				} else {
					result["Vector"] = "" //Takes too much room
					fmt.Println(result)
				}
			}
		}

	default:
		fmt.Println("Unknown command")
	}
}

// Enhanced help function with better formatting
func help() {
	fmt.Println(headerStyle.Render("AVAILABLE COMMANDS"))
	
	// Command categories
	fmt.Println(tableHeaderStyle.Render("Navigation & Setup:"))
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("exit"), "Exit the program")
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("url [url]"), "Set the url of the weaviate server")
	
	fmt.Println("\n" + tableHeaderStyle.Render("Search & Query:"))
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("search Class [words]"), "Search for objects in a class")
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("bm25 Class [words]"), "Search using BM25 (no vector)")
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("vector search Class [words]"), "Vector-based search")
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("ask [question]"), "Ask a question (Requires Q&A module)")
	
	fmt.Println("\n" + tableHeaderStyle.Render("Data Management:"))
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("delete class [classname]"), "Delete a class")
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("delete shard [shardname]"), "Delete a shard")
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("delete object [classname] [id]"), "Delete an object")
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("create object [classname] ..."), "Create a new object")
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("create class [classname] [vectorizer]"), "Create a new class")

	fmt.Println("\n" + tableHeaderStyle.Render("Information & Listing:"))
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("list classes"), "List all classes")
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("list shards [classname]"), "List shards for a class")
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("list objects [classname]"), "List objects in a class")
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("show class [classname]"), "Show class schema details")
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("info"), "Show info about the weaviate server")

	fmt.Println("\n" + tableHeaderStyle.Render("Advanced:"))
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("upload schema [file]"), "Upload schema from file")
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("download schema [file]"), "Save schema to file")
	fmt.Printf("  %s\t%s\n", propKeyStyle.Render("filter Class Operator Property Value"), "Search with filters")
	
	fmt.Println("\n" + infoStyle.Render("Add -json to any command to get JSON output"))
}
