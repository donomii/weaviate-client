# weaviate-client

An unofficial client library for go

Uses data structures instead of the builder pattern.

See included programs for examples.

## ripper

Website ripper.  Run with

    go run ripper.go -match-url weaviate https://weaviate.io   

